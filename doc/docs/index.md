## Documentation of the Tomography Framework Functionnalities

The tomography framework is designed to perform a full tomography with all necessary steps including dark images, reference images, images and metadata files
saving. Variants are a 180 degree and a 360 degree tomography.


### Main Acquisition Parameters

*For projection images:*

* Number of projections (`TOMO_N`)
* Exposure time per projection (`TOMO_EXPTIME`)
* Scanning mode which can be one of:
    * step scans
    * continuous scan
    * continuous scan without gap
* Latency time: The time to be added to camera readout time for safe triggering

*For reference images:* 

Reference images are beam images recorded with sample out of view. They are usually recorded before the scan and at the end of the scan.
The exposure time is the same as for projection images. 

Additionnal intermediate reference images can be acquired after a defined number of projections. The acquisition scan is then divided in *reference groups*.
One *reference group* consist of an acquisition of a portion of the full scan and the acquisition of the reference images.

* Number of projections between intermediate reference (`REF_ON`)
* Number of reference images to acquire (`REF_N`)

*For dark images:* 

Dark images are images recorded with shutter closed. They are usually recorded at the end of the scan.
The exposure time is the same as for projection images.

* Number of dark images to record (`DARK_N`)

![scan point time](images/tomo_scan_rot.svg)