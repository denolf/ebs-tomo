from distutils.core import setup

setup(name='Tomo',
      version='0.1.0',
      description='Fast Tomography',
      author='Clemence Muzelle',
      author_email='muzelle@esrf.fr',
      url='https://gitlab.esrf.fr/tomo/ebs-tomo',
      
      package_dir = {'ID19': 'tomo/beamline/ID19',
                     'ID11': 'tomo/beamline/ID11'
                    },
      packages=['tomo', 'tomo.pcotomo', 'ID19', 'ID11'],
     )
