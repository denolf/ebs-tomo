from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.chain import AcquisitionMaster, AcquisitionChannel
import gevent
from gevent import event
import numpy

         
class SoftTriggerMusstAcquisitionMaster(AcquisitionMaster):
    """
    Class to generate triggers by musst without any program   
    """
    def __init__(self, musst_device, ctime):
        """
        Parameters
        ----------
        time : float
            count time
        musst : musst controller
            musst device object
        """
        AcquisitionMaster.__init__(self, musst_device, name="musst_step")
        self.time = ctime
        self.musst = musst_device


    def prepare(self):
        pass
        
    def start(self):
        pass
    
    def trigger(self):
        """
        Runs RUNCT command ---> generates 2 pulses of 100 ms on outA at 
                               the beginning and the end of counting + 
                               a gate on outB of ctime width
        """
        #print("start trigger musst")
        
        self.musst.ct(self.time)

    def stop(self):
        pass
        

#object needed by the default chain config to know how to create musst master in case of step-by-step scans
#class MusstStepScan(object):
    #def __init__(self, tomo_name, tomo_config):
        #self.musst = tomo_config["musst"]

    #def create_master_device(self, scan_pars, **settings):
        #ctime = scan_pars['count_time']

        #master = SoftTriggerMusstAcquisitionMaster(self.musst, ctime)

        #return master 

# linear motor master with one iteration per reference group
class StepMotorMaster(AcquisitionMaster):
    """
    Class to handle step scans with reference groups
    """
    def __init__(self,motor,start,end,npoints):
        """
        motor : Axis
            scanning motor 
        start : float or list
            motor start position or motor start positions of each group if reference groups exist
        end : float
            motor end position
        npoints : int
            number of steps
        """
        AcquisitionMaster.__init__(self,motor,trigger_type=AcquisitionMaster.SOFTWARE,
                                    npoints=npoints)
        
        if isinstance(start,list):
            self.step = (end-start[0])/npoints
        else:
            self.step = (end-start)/npoints
        
        self.start_pos = start
        self.end_pos = end   
        
        # variables that will be updated for each group
        self.start_group = None
        self.end_group = None
        
        self.channels.append(AcquisitionChannel(f"axis:{motor.name}",
                                                numpy.double, (), unit=motor.unit))

    def __iter__(self):
        """
        Generates one motor movement per group
        """
        self._iter_index = 0
        if isinstance(self.start_pos,list):
            for index in range(len(self.start_pos)):
                self.start_group = self.start_pos[index]
                try:
                    self.end_group = self.start_pos[index+1]
                except:
                    self.end_group = self.end_pos
                yield self
                self._iter_index += 1
        else:
            self.start_group = self.start_pos
            self.end_group = self.end_pos
            while True:
                yield self
                self._iter_index += 1
                if not self.parent:
                    break


    def prepare(self):
        """
        Moves to start position of the group
        """
        # the device of AcqMaster is the motor
        self.device.move(self.start_group)

    def start(self):
        if self.parent is None:
            self.trigger()

        
    def stop(self):
        pass    
        
    def trigger(self):
        """
        For each group, moves to each step position to trigger and wait slaves
        """
        
        # Emit first motor position
        self.channels.update_from_iterable([self.device.position])
        
        for nb_step in range(1,int((self.end_group-self.start_group)/self.step)+1):

            pos = self.start_group + nb_step*self.step
            
            # Start trigger slaves
            self.trigger_slaves()
            
            # Wait end slaves trigger
            self.wait_slaves()
            
            # wait slave are ready for next trigger
            #self.wait_slaves_ready()
            
            self.device.move(pos)
            
            # Emit motor position
            self.channels.update_from_iterable([pos])
        
        
        

    
    
