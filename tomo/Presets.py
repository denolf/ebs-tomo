from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset,ChainIterationPreset
from tomo.TomoShutter import ShutterType

class DarkShutterPreset(ScanPreset):
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        self.shutter.init()
        
    def prepare(self,scan):
        print(f"Preparing shutter for {scan.name}\n")
        print(f"Closing the dark shutter")
        self.shutter.dark_shutter_close()
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        print(f"Opening the dark shutter")
        self.shutter.dark_shutter_open()
        
        
class FastShutterPreset(ScanPreset):
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        self.shutter.init()
        
    def prepare(self,scan):
        if self.shutter.shutter_used != ShutterType.NONE:
            print(f"Preparing shutter for {scan.name}\n")
            if self.shutter.shutter_used == ShutterType.CCD:
                print(f"Fast shutter managed by detector")
                self.shutter.fast_shutter_open()
        
    def start(self,scan):
        if self.shutter.shutter_used == ShutterType.SOFT:
            print(f"Opening the fast shutter")
            self.shutter.fast_shutter_open()
        
    def stop(self,scan):
        if self.shutter.shutter_used == ShutterType.SOFT:
            print(f"Closing the fast shutter")
            self.shutter.fast_shutter_close()
        

class DefaultChainFastShutterPreset(ChainPreset):
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        
    class Iterator(ChainIterationPreset):
        def __init__(self,shutter,iteration_nb):
            self.shutter = shutter
            self.iteration = iteration_nb
            
        def prepare(self):
            if self.shutter.shutter_used != ShutterType.NONE:
                print(f"Preparing shutter\n")
                if self.shutter.shutter_used == ShutterType.CCD:
                    print(f"Fast shutter managed by detector")
                    self.shutter.fast_shutter_open()
        
        def start(self):
            if self.shutter.shutter_used == ShutterType.SOFT:
                print(f"Opening the fast shutter")
                self.shutter.fast_shutter_open()
    
        def stop(self):
            if self.shutter.shutter_used == ShutterType.SOFT:
                print(f"Closing the fast shutter")
                self.shutter.fast_shutter_close()
                
    def get_iterator(self,acq_chain):
        iteration_nb = 0
        while True:
            yield DefaultChainFastShutterPreset.Iterator(self.shutter,iteration_nb)
            iteration_nb += 1


class CommonHeaderPreset(ScanPreset):
    def __init__(self, tomo_ccd, header):
        self.tomo_ccd = tomo_ccd
        self.header = header
        
    def prepare(self,scan):
        print(f"Preparing common image header for {scan.name}\n")
        if not self.header:
            self.tomo_ccd.reset_image_header()
        else:
            print(f"write common image header")
            self.tomo_ccd.image_header(self.header)
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        print(f"reset common image header")
        self.tomo_ccd.reset_image_header()


class ReferenceMotorPreset(ScanPreset):
    def __init__(self, reference):
        self.reference= reference
        
    def prepare(self,scan):
        print(f"Move sample out of beam for {scan.name}\n")
        self.reference.move_out()
        print ("moved out")
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        print(f"Move sample back in beam")
        self.reference.move_in()
        print ("moved in")
