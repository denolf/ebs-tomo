import sys
import gevent
import numpy as np
import time
import datetime

import bliss
from bliss import setup_globals
from bliss.common import axis
from bliss.common import event
from bliss.common import session, scans
from bliss.scanning.chain import AcquisitionChain, AcquisitionChannel, AcquisitionMaster
from bliss.scanning.scan import Scan,ScanState, DataWatchCallback, WatchdogCallback
from bliss.data.node import is_zerod



class ColorTags:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

def __color_message(tag, msg):
    return "{0}{1}{2}".format(tag, msg, ColorTags.END)

def PURPLE(msg):
   return __color_message(ColorTags.PURPLE, msg)
def CYAN(msg):
   return __color_message(ColorTags.CYAN, msg)
def DARKCYAN(msg):
   return __color_message(ColorTags.DARKCYAN, msg)
def BLUE(msg):
   return __color_message(ColorTags.BLUE, msg)
def GREEN(msg):
   return __color_message(ColorTags.GREEN, msg)
def YELLOW(msg):
   return __color_message(ColorTags.YELLOW, msg)
def RED(msg):
   return __color_message(ColorTags.RED, msg)
def UNDERLINE(msg):
   return __color_message(ColorTags.UNDERLINE, msg)
def BOLD(msg):
   return __color_message(ColorTags.BOLD, msg)


#def BOLD(msg):
#    return "\033[1m{0}\033[0m".format(msg)
    
    
    
    

class ScanDisplay(DataWatchCallback):
    """
    Class to display data during a tomo scan
    """

    HEADER = (
        "Scan {} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}"
    )

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, trigger_name=None, motors=list(), limas=list()):
        """
        Initialisation of all scan members
        """
        self.__motors = motors
        self.__motor_names = [motor.name for motor in motors]
        self.__limas = limas
        self.__trig_name = trigger_name

    def on_state(self, state):
        """
        If True "on_scan_data" will be called at 
        each scan state: PREPARING, STARTING and STOPPING
        """
        return True

    def on_scan_new(self, scan, info):
        """
        Displays scan info: scan number, data saving path, user name, ...
        Called when the scan is about to start
        
        scan -- is the scan object
        info -- is the dict of information about this scan
        """
        # displays scan number / total number of scans to see the acquisition progression
        if info.get('technique').get('scan').get('sequence') == 'tomo:zseries':
            SCAN_NB = f"{info.get('scan_nb')} / {int(info.get('scan_nb'))+self.NB_SCANS}"
        else:
            SCAN_NB = info.get('scan_nb')
            
        print(self.HEADER.format(SCAN_NB,**info))
        self.__state = None
        self.__infos = dict()
        for name in self.__motor_names:
            #print ("Motor = %s\n" % name)
            self.__infos[name] = "----.---"
        if self.__trig_name is not None:
            self.__infos["trig"] = 0
        for lima in self.__limas:
            self.__infos[lima.name] = 0
        self.__start_time = 0
        self.__last_saved = 0
        self.__data_saving_rate = 0
        self.__new_trigger = False
        
    def on_scan_end(self, info):
        """
        Displays scan execution time.
        Called at the end of the scan.
        """

        start = datetime.datetime.fromtimestamp(info["start_timestamp"])
        end = datetime.datetime.fromtimestamp(time.time())
        msg = "\nFinished (took {0})\n".format(end - start)
        print(msg)

    def on_scan_data(self, data_events, data_nodes, info):
        """
        This callback is called when new data is emitted
        Displays:
            - motor position
            - trigger number
            - last image acquired
            - last image saved
            - difference between trigger number and last image acquired
            - data saving rate in MB/s
        """
        # look for scan state
        state = info.get("state", None)
        if state != self.__state:
            if state == ScanState.STARTING:
                self.__start_time = datetime.datetime.fromtimestamp(time.time())
            self.__state = state
            state_msg = self.STATE_MSG.get(state, None)
            if state_msg is not None:
                print("\n{0} ...".format(state_msg))
            
        if self.__state == ScanState.PREPARING:
            # print current motor positions
            msg = ""
            for motor in self.__motors:
                name = motor.name
                value = "{0:8.3f}".format(motor.position)
                msg += "{0} {1}  ".format(BOLD(name), value)
                self.__infos[name] = value
            if len(msg):
                print(msg + "\r", end="")
            return

        if self.__state == ScanState.STARTING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    data_node = data_nodes.get(acqdev)
                    if is_zerod(data_node):
                        channel_name = data_node.name
                        #print("    data_name {0}".format(channel_name))
                        prefix,_,name    = channel_name.rpartition(":")
                        
                        # get rid of any suffix for calculated channels
                        #if 'pos_' in name:
                            #prefix,_,name = name.rpartition("_") 
                        if name in self.__motor_names:
                            last_pos = data_node.get(-1)
                            self.__infos[name] = "{0:8.3f}".format(last_pos)
                        if channel_name == self.__trig_name:
                            self.__infos["trig"] = len(data_node)
                            self.__new_trigger = True
                                
            # print last images acquired and saved
            for cam in self.__limas:
                if cam.camera_type.lower() == 'pco' and 'dimax' in cam.camera.cam_name.lower():
                    last_acq = cam.camera.last_img_recorded
                else:
                    last_acq = cam._proxy.last_image_ready + 1
                last_saved = cam._proxy.last_image_saved + 1
                diff_trigger = self.__infos["trig"] - last_acq
                depth = cam._proxy.image_sizes[1]
                width = cam._proxy.image_sizes[2]
                height = cam._proxy.image_sizes[3]
                mbsize = depth*width*height/1024/1024
                if self.__new_trigger:
                    diff_saved = last_saved - self.__last_saved
                    self.__last_saved = last_saved
                    end_time = datetime.datetime.fromtimestamp(time.time())
                    dtime = end_time - self.__start_time
                    dtime_in_sec = dtime.total_seconds()
                    self.__data_saving_rate = int(mbsize*last_saved/dtime_in_sec) 
                    self.__new_trigger = False
                
                self.__infos[cam.name] = "{0} (saved {1} diff {2} data saving rate {3} MB/s)".format(last_acq, last_saved, diff_trigger, self.__data_saving_rate)

            
            msg = ""
            for (name, value) in self.__infos.items():
                msg += "{0} {1}  ".format(BOLD(name), value)
                
            if len(msg):
                print(msg + "\r", end="")
                
            for cam in self.__limas:
                if cam.camera_type.lower() == 'pco' and 'dimax' in cam.camera.cam_name.lower():
                    last_acq = cam.camera.last_img_recorded
                else:
                    last_acq = cam._proxy.last_image_ready + 1
                last_saved = cam._proxy.last_image_saved + 1
                diff = last_acq - last_saved
                if cam._proxy.saving_mode == 'AUTO_FRAME':
                    if last_acq == cam._proxy.acq_nb_frames and diff != 0:
                        # erase line and go to beginning of line
                        print('\033[2K\033[1G', end="")
                        while diff != 0:
                            time.sleep(0.5)
                            if 'pco' in cam.camera_type.lower() and 'dimax' in cam.camera.cam_name.lower():
                                last_acq = cam.camera.last_img_recorded
                            else:
                                last_acq = cam._proxy.last_image_ready + 1
                            last_saved = cam._proxy.last_image_saved + 1
                            diff = last_acq - last_saved
                            msg = "Saving in progress... {0} saved {1}".format(BOLD(cam.name), last_saved)
                            print(msg + "\r", end="")
                        end_time = datetime.datetime.fromtimestamp(time.time())
                        dtime = end_time - self.__start_time
                        dtime_in_sec = dtime.total_seconds()
                        self.__data_saving_rate = int(mbsize*last_saved/dtime_in_sec) 
            return


        if self.__state == ScanState.STOPPING:
            # print last motor position
            msg = ""
            for motor in self.__motors:
                msg += "{0} {1:8.3f}  ".format(BOLD(motor.name), motor.position)
            if len(msg):
                print(msg + "\r", end="")
            return
                
class ScanWatchdog(WatchdogCallback):
    
    def __init__(self, musst_card, trigger_name=None, limas=list(), trig_difference=2):
        """
        watchdog_timeout -- is the maximum calling frequency of **on_timeout**
        method.
        """
        self.__trig_difference = trig_difference
        self.__watchdog_timeout = 20.0
        self.__musst = musst_card
        self.__limas = limas
        self.__trig_name = trigger_name
        self.__stop_reason = ''
    @property
    def timeout(self):
        return self.__watchdog_timeout

    def on_timeout(self):
        """
        This method is called when **watchdog_timeout** elapsed it means
        that no data event is received for the time specified by
        **watchdog_timeout**
        """
        for cam in self.__limas:
            if cam.camera_type.lower() == 'pco' and 'dimax' in cam.camera.cam_name.lower():
                last_acq = cam.camera.last_img_recorded
            else:
                last_acq = cam._proxy.last_image_ready + 1
            #check if lima is saving    
            if last_acq != cam._proxy.acq_nb_frames:
                self.__stop_reason = f'No data received since {self.__watchdog_timeout} seconds'
                raise Exception            
        

    def on_scan_new(self, scan, scan_info):
        """
        Called when scan is starting
        """
        self.__state = None
        self.__trig = 0
    
    def on_scan_data(self, data_events, nodes, scan_info):
        """
        Called when new data are emitted by the scan.  This method should
        raise en exception to stop the scan.  All exception will
        bubble-up exception the **StopIteration**.  This one will just
        stop the scan.
        """
        # look for scan state
        state = scan_info.get("state", None)
        if state != self.__state:
            self.__state = state
        
        if self.__state == ScanState.STARTING or self.__state == ScanState.STOPPING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    node = nodes.get(acqdev)
                    if is_zerod(node):
                        channel_name = node.name
                        # print("    data_name {0}".format(channel_name))
                        prefix,_,name = channel_name.rpartition(":")
                        if channel_name == self.__trig_name:
                            self.__trig = len(node)
                            
                            
            for cam in self.__limas:
                if cam.camera_type.lower() == 'pco' and 'dimax' in cam.camera.cam_name.lower():
                    last_acq = cam.camera.last_img_recorded
                else:
                    last_acq = cam._proxy.last_image_ready + 1
                diff = self.__trig - last_acq
                if self.__trig_difference > 0 and diff > self.__trig_difference and cam._proxy.acq_status == 'Running':
                    self.__stop_reason = f'\nSYNCHRO LOST: Detector {cam.name} did not record all images({last_acq}/{self.__trig})\n'
                    raise Exception
        
        return

    def on_scan_end(self, scan_info):
        if len(self.__stop_reason):
            print(BOLD(self.__stop_reason) + "\n")


class LimaTakeDisplay(DataWatchCallback):
    HEADER = (
        "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}"
    )

    def __init__(self, *lima_objs):
        self.__limas = lima_objs

    def on_state(self, state):
        return True

    def on_scan_new(self, scan, info):
        print(self.HEADER.format(**info))
        self.__state = None
        self.__infos = dict()
        self.__save_flag = info.get("save", False)
        
        for lima in self.__limas:
            self.__infos[lima.name] = 0

    def on_scan_end(self, info):
        msg = self.__update_cam_infos()
        print(msg)
        start = datetime.datetime.fromtimestamp(info["start_timestamp"])
        end = datetime.datetime.fromtimestamp(time.time())
        msg = "Finished (took {0})\n".format(end - start)
        print(msg)

    def __update_cam_infos(self):
        for cam in self.__limas:
            last_acq = cam._proxy.last_image_ready + 1
            msg = "acq #{0}".format(last_acq)
            if self.__save_flag:
                last_saved = cam._proxy.last_image_saved + 1
                msg += " save #{0}".format(last_saved)
            last_status = cam._proxy.acq_status
            if last_status == "Ready":
                msg = BOLD(msg)
            elif last_status == "Fault":
                msg = RED(msg)
            self.__infos[cam.name] = msg
        msg = ""
        for (name, value) in self.__infos.items():
            msg += "{0} {1}  ".format(BOLD(name), value)
        return msg

    def on_scan_data(self, data_events, data_nodes, info):
        # look for scan state
        state = info.get("state", None)
        if state != self.__state:
            self.__state = state
            if state == ScanState.PREPARING:
                msg = "Preparing "
                for lima in self.__limas:
                    msg += "{0} ".format(lima.name)
                print(msg + "...")
            if state == ScanState.STARTING:
                print("Running ...")
        if state == ScanState.STARTING:
            # print last images acquired and saved
            msg = self.__update_cam_infos()
            print(msg + "\r", end="")
