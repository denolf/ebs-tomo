import sys
import gevent
import numpy as np
import time
import datetime
from importlib import import_module

from bliss import setup_globals, global_map, current_session
from bliss.common import mapping, scans
from bliss.common import session
from bliss.common.axis import AxisState
from bliss.common.scans import pointscan, DEFAULT_CHAIN
from bliss.scanning.group import Sequence
from bliss.common.logtools import log_info, log_debug, log_error 
from bliss.common.cleanup import cleanup, error_cleanup, capture_exceptions, axis as cleanup_axis
from bliss.shell.standard import umv
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.config.static import get_config

import tomo
from tomo.TomoParameters import TomoParameters
from tomo.TomoMusst import TriggerType
from tomo.Presets import DarkShutterPreset, FastShutterPreset, CommonHeaderPreset, ReferenceMotorPreset
from tomo.TomoCounters import TomoCounters


from enum import Enum
class ScanType(Enum):
    STEP  = 0
    CONTINUOUS_SOFT = 1
    CONTINUOUS_HARD = 2
    SWEEP = 3
    


class Tomo(TomoParameters):
    def __init__(self, tomo_name, config):
        # init logging
        self.name = tomo_name
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        self.config = config
        self.tomo_name = tomo_name
        
        #print(config)

        # Initialise the hardware necessary for the tomo scans and access
        # the hardware onece to be sure that the initialization is done
        # and the equipment connected.

        # Init and test the rotation axis
        self.rotation_axis = config['rotation']['axis']
            
        print ("Rotation axis used: %s" % self.rotation_axis.name)
        print ("Rotation axis position: %f" % self.rotation_axis.position)

        # Init and test detector axis
        self.detector_axis = config["detector_x_axis"]
        print ("Detector axis used: %s" % self.detector_axis.name)
        print ("Detector axis position: %f" % self.detector_axis.position)

        # Init and test the detector
        self.tomo_ccd = tomo.TomoCcd(self.tomo_name, config)
        print ("Detector used: %s" % self.tomo_ccd.detector.name)
        #print ("Detector parameters: %s" % self.tomo_ccd.detector)
    
        ## Init and test the shutter
        self.shutter = tomo.TomoShutter(self.tomo_name, config, self.tomo_ccd)
        print ("Shutter used: %s" % self.shutter.shutter_used)
        
        ## Init and test the optic
        self.list_opt = config['optic']
        
        param_name = tomo_name+f':optic_parameters'
        optic_defaults = {}
        optic_defaults['active_optic'] = None

        self._tomo_optic = tomo.TomoOptic(self.tomo_name, config, param_name, optic_defaults)

        active_optic = None
        if self._tomo_optic.parameters.active_optic is not None:
            active_optic = get_config().get(self._tomo_optic.parameters.active_optic)
          
        if active_optic is not None and active_optic in self.list_opt:
            self.optic = active_optic
        else:
            self.optic = self.list_opt[0]
            
        self._tomo_optic.parameters.active_optic = self.optic.name
        
        print ("Optic used: %s" % self.optic.description)

        # Init and test the reference motors
        self.reference = tomo.TomoRefMot(self.tomo_name, config, self)

        # Init and test the lateral alignment motor
        self.lateral_align_axis = config['halftomo']["lat_align_axis"]

        # Init and test the musst
        self.musst_card = config['musst']
        motor = self.rotation_axis
        self.tomo_musst = tomo.TomoMusst(self.tomo_name, self.musst_card, motor)
        print("Musst channel used: %s" % self.tomo_musst.mot_chan)

        # Init the data saving
        self.saving = tomo.TomoSaving(self.tomo_name, config, self.tomo_ccd)
        self.saving.show_config()
        
        # Init the meta data object
        self.meta_data = tomo.TomoMetaData(self.tomo_name, self)

        ## Init and test the additional counters
        try:
            self.counters = config['additional_counters']
            # check the object type
            if not isinstance(self.counters, TomoCounters):
                self.counters = None
        except:
            self.counters = None
        
        if self.counters == None:
            print(f"No additional counters specified for {self.name}!")
        else:
            print (f"Additional counters from class {self.counters.__class__.__name__}")
        
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':scan_parameters'
        scan_defaults = {}
        scan_defaults['tomo_n']  = 1500
        scan_defaults['ref_on']  = 100
        scan_defaults['ref_n']   = 21
        scan_defaults['dark_n']  = 20
        scan_defaults['exposure_time'] = 0.5
        scan_defaults['latency_time']  = 0.2
        scan_defaults['start_pos']     = 0.0
        scan_defaults['end_pos']       = 360.0
        scan_defaults['half_acquisition'] = False
        scan_defaults['scan_type'] = ScanType.STEP
        scan_defaults['trigger_type'] = TriggerType.TIME
        scan_defaults['energy']    = 0.0
        scan_defaults['source_sample_distance']   = 145000.0
        scan_defaults['sample_detector_distance'] = 0.0
        scan_defaults['no_reference_groups'] = False
        scan_defaults['ref_images_at_start']  = False
        scan_defaults['ref_images_at_end']    = True
        scan_defaults['dark_images_at_start'] = True
        scan_defaults['dark_images_at_end']   = False
        scan_defaults['dark_images_in_accumulation'] = True
        scan_defaults['no_return_images']     = False
        scan_defaults['return_images_aligned_to_refs'] = False
        scan_defaults['return_to_start_pos']  = True
        
        # Initialise the TomoParameters class
        super().__init__(param_name, scan_defaults)
        
        # Init and test preset
        beamline             = config['beamline']
        self.sequence_preset = config['sequence_preset']
        
        self.tomo_tools = tomo.TomoTools(self)
        
        self.scan_prepare = False
        self.fasttomo = tomo.FastTomo(self)
        self.halftomo = tomo.HalfTomo(self)
        self.sequence  = 'tomo:basic'
        self.field_of_view = 'Full'
        self.active_tomo = self.fasttomo
        self.tomo_scan.acc_margin = config['rotation'].get('acc_margin',0.0)
        self.tomo_scan.use_step_size = config['rotation'].get('use_step_size',False)
        self.scan_motors = [self.rotation_axis]
        self.in_pars = {}

        
        log_info(self,"__init__() leaving")
        
    def __info__(self):
        info_str  = f"{self.name} info:\n"
        info_str += f"Configuration\n"
        info_str += f"  rotation axis = {self.rotation_axis.name}\n"
        info_str += f"  detector axis = {self.detector_axis.name}\n"
        info_str += f"  detector      = {self.tomo_ccd.detector.name}\n"
        info_str += f"  optic         = {self.optic.description}\n"
        info_str += f"  shutter       = {self.shutter.shutter_used}\n"
        
        info_str += f"Scan\n"
        info_str += f"  tomo_n        = {self.parameters.tomo_n}\n"
        info_str += f"  ref_on        = {self.parameters.ref_on}\n"
        info_str += f"  ref_n         = {self.parameters.ref_n}\n"
        info_str += f"  dark_n        = {self.parameters.dark_n}\n"
        info_str += f"  exposure_time = {self.parameters.exposure_time}\n"
        info_str += f"  latency_time  = {self.parameters.latency_time}\n"
        info_str += f"  start_pos     = {self.parameters.start_pos}\n"
        info_str += f"  end_pos       = {self.parameters.end_pos}\n"
        info_str += f"  scan_type        = {self.parameters.scan_type}\n"
        info_str += f"  trigger_type     = {self.parameters.trigger_type}\n"
        info_str += f"  half_acquisition = {self.parameters.half_acquisition}\n"
        info_str += f"  source_sample_distance   = {self.parameters.source_sample_distance}\n"
        info_str += f"  sample_detector_distance = {self.parameters.sample_detector_distance}\n"
        info_str += f"  energy = {self.parameters.energy}\n"
        return info_str
    
    def select_optic(self):
        values = list()
        for i in range(len(self.list_opt)):
            if self.list_opt[i] == self.optic:
                defval = i
            values.append((i,self.list_opt[i].name))
            
        dlg = UserChoice(values=values,defval=defval)
        ret = BlissDialog([[dlg],], title="Optic Select").show()
        
        if ret != False:
            self.optic = self.list_opt[ret[dlg]]
            self._tomo_optic.parameters.active_optic = self.optic.name
                
        print ("Optic used: %s" % self.optic.description)
    
    def select_detector(self):
        
        self.tomo_ccd.select_detector()
    
    def setup(self):
        """
        Set-up the tomo scan and all its sub objects like detector, 
        optic, shutter ,saving, etc.
        """
        value_list = [("scan",   "Scan_Parameter Setup"),
                      ("ccd",    "Detector Setup"),
                      ("optic",  "Optic Setup"),
                      ("reference", "Reference Setup"),
                      ("saving", "Saving Setup"),
                      ("flags",  "Scan Option Setup"),
                      ("type",   "Scan Type Setup"),
                      ("exit",   "Exit")]
        
        ret     = True
        choice  = "scan"
        default = 0

        while ret != False and choice != "exit":
            dlg1 = UserChoice(values=value_list, defval=default)
        
            ret = BlissDialog( [ [dlg1], ], title='Tomo Setup').show()
        
            # returns False on cancel
            if ret != False:
                choice = ret[dlg1]
                
                if choice == "scan":
                    self.scan_setup()
                if choice == "flags":
                    self.flag_setup()
                if choice == "type":
                    self.type_setup()
                if choice == "ccd":
                    self.tomo_ccd.setup()
                if choice == "optic":
                    self.select_optic()
                    self.optic.setup()
                if choice == "reference":
                    self.reference.setup()
                if choice == "saving":
                    self.saving.setup()
                    
                for i in range (0, len(value_list)):
                    if choice == value_list[i][0]:
                        default = i
                        break
    
        
    def scan_setup(self):
        """
        Set-up the main tomo scan parameters and the scan synchronization type
        """
        
        dlg_tomo_n  = UserIntInput(label="Number of Projections?", defval=self.parameters.tomo_n)
        dlg_expo    = UserFloatInput(label="Detector Exposure Time [s]?", defval=self.parameters.exposure_time)
        # calculate and display the minimum latency time
        min_latency_time = self.tomo_tools.calculate_latency_time(self.tomo_musst)
        dlg_min     = UserMsg(label=("The minimum latency time is " + str(min_latency_time) + " [s]"))
        dlg_latency = UserFloatInput(label="Latency Time [s]?", defval=self.parameters.latency_time)
        
        dlg_dark_n  = UserIntInput(label="Number of Dark Images?", defval=self.parameters.dark_n)
        dlg_ref_n   = UserIntInput(label="Number of Reference Images?", defval=self.parameters.ref_n)
        # Only ask for ref_on if reference groups are enabled
        if self.parameters.no_reference_groups == False:
            dlg_ref_on  = UserIntInput(label="Reference Images after how many projections?", defval=self.parameters.ref_on)
        else:
            dlg_ref_on  = UserMsg(label="Reference Groups are Disabled")
        
        dlg_sdd     = UserFloatInput(label="Sample to Detector Distance [mm]?", defval=self.parameters.sample_detector_distance)
        dlg_ssd     = UserFloatInput(label="Source to Sample Distance [mm]?", defval=self.parameters.source_sample_distance)
        
        ct1 = Container( [dlg_tomo_n, dlg_expo, dlg_min, dlg_latency], title="Main Parameters" )
        ct2 = Container( [dlg_dark_n, dlg_ref_n, dlg_ref_on], title="Dark and Reference Images" )
        ct3 = Container( [dlg_sdd, dlg_ssd], title="Distances" )
        
        ret = BlissDialog( [[ct1], [ct2], [ct3]], title='Tomo Scan Setup').show()
        
        # returns False on cancel
        if ret != False:
            self.parameters.tomo_n          = int(ret[dlg_tomo_n])
            self.parameters.exposure_time   = float(ret[dlg_expo])
            self.parameters.latency_time    = float(ret[dlg_latency])
            self.parameters.dark_n          = int(ret[dlg_dark_n])
            self.parameters.ref_n           = int(ret[dlg_ref_n ])
            if self.parameters.no_reference_groups == False:
                self.parameters.ref_on      = int(ret[dlg_ref_on])
            self.parameters.sample_detector_distance = float(ret[dlg_sdd])
            self.parameters.source_sample_distance   = float(ret[dlg_ssd])

    
    def flag_setup(self):
        """
        Set-up the tomo scan options for dark images, reference images, return images and others. 
        All options can be True or False
        """
        dlg1 = UserCheckBox(label="Dark Images at Start", defval=self.parameters.dark_images_at_start)
        dlg2 = UserCheckBox(label="Dark Images at End",   defval=self.parameters.dark_images_at_end)
        dlg3 = UserCheckBox(label="Dark Images in Accumulation Mode", defval=self.parameters.dark_images_in_accumulation)
        dlg4 = UserCheckBox(label="Dark Images with Safety Shutter", defval=self.shutter.parameters.dark_images_with_beam_shutter)
        
        dlg5 = UserCheckBox(label="Reference Images at Start", defval=self.parameters.ref_images_at_start)
        dlg6 = UserCheckBox(label="Reference Images at End",   defval=self.parameters.ref_images_at_end)
        dlg7 = UserCheckBox(label="No Reference Groups",       defval=self.parameters.no_reference_groups)
        
        dlg8 = UserCheckBox(label="No Images on Scan Return", defval=self.parameters.no_return_images)
        dlg9 = UserCheckBox(label="Return Images aligned with Reference Groups", defval=self.parameters.return_images_aligned_to_refs)
        
        dlg10  = UserCheckBox(label="Half Acquisition",   defval=self.parameters.half_acquisition)
        dlg11 = UserCheckBox(label="Return to Start Position", defval=self.parameters.return_to_start_pos)
        
        ct1 = Container( [dlg1, dlg2, dlg3, dlg4], title="Dark Images" )
        ct2 = Container( [dlg5, dlg6, dlg7], title="Reference Images" )
        ct3 = Container( [dlg8, dlg9], title="Return Images" )
        ct4 = Container( [dlg10, dlg11], title="Others" )
        
        ret = BlissDialog( [[ct1], [ct2], [ct3], [ct4]], title='Tomo Option Setup').show()
        
        # returns False on cancel
        if ret != False:
            self.parameters.dark_images_at_start          = ret[dlg1]
            self.parameters.dark_images_at_end            = ret[dlg2]
            self.parameters.dark_images_in_accumulation   = ret[dlg3]
            self.shutter.parameters.dark_images_with_beam_shutter = ret[dlg4]
            self.parameters.ref_images_at_start           = ret[dlg5]
            self.parameters.ref_images_at_end             = ret[dlg6]
            self.parameters.no_reference_groups           = ret[dlg7]
            self.parameters.no_return_images              = ret[dlg8]
            self.parameters.return_images_aligned_to_refs = ret[dlg9]
            self.parameters.half_acquisition              = ret[dlg10]
            self.parameters.return_to_start_pos           = ret[dlg11]
            
            
    def type_setup(self):
        """
        Set-up the tomo scan type. 
        The possibilities are:
        1.) Step scan
        2.) Software triggered continuous scan - The triggers are calculated in position
        3.) Hardware triggered continuous scan - The triggers generated by the MUSST card can be in position or in time
        4.) Sweep scan 
        """

        values = [(0,"Step Scan"), (1,"Continuous Scan - Software Triggered"), (2,"Continuous Scan - Hardware Triggered"), (3,"Sweep Scan")]
        dlg_scan_type    = UserChoice(label="Scan Type?", values=values, defval=self.parameters.scan_type.value)
        
        values = [(0,"Position"), (1,"Time")]
        dlg_trigger_type = UserChoice(label="Musst Triggered In?", values=values, defval=self.parameters.trigger_type.value)
        
        ret = BlissDialog( [[dlg_scan_type],[dlg_trigger_type]], title='Tomo Scan Type Setup').show()
        
        # returns False on cancel
        if ret != False:
            self.parameters.scan_type       = ScanType(ret[dlg_scan_type])
            self.parameters.trigger_type    = TriggerType(ret[dlg_trigger_type])
            
    def half_acquisition_setup(self):
        """
        Set-up the sample size and the number of acquired images for half-acquisition tomo
        """

        values = [(0,"Field-of-view factor"), (1,"Millimeters"), (2,"Pixels")]
        dlg_size_type = UserChoice(label="Sample Size Type?", values=values, defval=0)
        
        dlg_size_value  = UserFloatInput(label="Sample Size Value?", defval=self.halftomo.parameters.sample_size_in_fov_factor)
        
        ret = ret2 = False
        
        while ret2 is False:
        
            ret = BlissDialog( [[dlg_size_type], [dlg_size_value]], title='Tomo Half Acquisition Setup (1/2)').show()

            # returns False on cancel
            if ret != False:
                if ret[dlg_size_type] == 0:
                    self.halftomo.parameters.sample_size_in_fov_factor = ret[dlg_size_value]
                    self.halftomo.parameters.sample_size_in_mm = 0.0
                    self.halftomo.parameters.sample_size_in_pixels = 0
                if ret[dlg_size_type] == 1:
                    self.halftomo.parameters.sample_size_in_mm = ret[dlg_size_value]
                    self.halftomo.parameters.sample_size_in_fov_factor = 0
                    self.halftomo.parameters.sample_size_in_pixels = 0
                if ret[dlg_size_type] == 2:
                    self.halftomo.parameters.sample_size_in_pixels = int(ret[dlg_size_value])
                    self.halftomo.parameters.sample_size_in_fov_factor = 0
                    self.halftomo.parameters.sample_size_in_mm = 0.0
            
                pixel_size = self.tomo_ccd.calculate_image_pixel_size(self.optic.magnification) / 1000
                ccd_cols = self.tomo_ccd.get_image_parameters()['size'][0]
                col_bin = self.tomo_ccd.get_image_parameters()['binning'][0]
                
                self.halftomo.sample_size_configured = self.halftomo.configure_sample_size(ccd_cols,pixel_size)
                
                if not self.halftomo.sample_size_configured:
                    print("You need to define sample size")
                    return False
                    
                if self.halftomo.parameters.sample_size_in_fov_factor <= 1:
                    print('Error: Whole sample fits into CCD field of view!')
                    print('No need for half-acquisition!')
                    return False
                elif self.halftomo.parameters.sample_size_in_fov_factor > 1.98:
                    print('Error: Sample too big!')
                    print('Half of the sample does not fit in CCD field of view!')
                    return False
            
                fov_factor = self.halftomo.parameters.sample_size_in_fov_factor     
                fov_shift = fov_factor / 2 - 0.5    
                
                self.halftomo.parameters.number_of_360_images = (2723.8 + 2.5476 * (fov_shift*2048)) / col_bin
                self.halftomo.parameters.number_of_360_images = int((int(self.halftomo.parameters.number_of_360_images) + 99) / 100) * 100
                
                dlg_default     = UserMsg(label=("The default number of 360 images is " + str( self.halftomo.parameters.number_of_360_images)))
                dlg_nb_images = UserIntInput(label="Number of 360 images?", defval=self.halftomo.parameters.number_of_360_images)
                
                ret2 = BlissDialog( [[dlg_default], [dlg_nb_images]], title='Tomo Half Acquisition Setup (2/2)').show()
                
                if ret2 != False:
                    self.halftomo.parameters.number_of_360_images = ret2[dlg_nb_images]
                    
                self.halftomo.setup_done = True    
            
            else:
                break
        


    def return_scan(self, scan_sequence=None, header={}):
        """
        Acquires static images on scan return.
        By default, images are taken every 90 degrees and aligned to reference groups 
        if the option 'return_images_aligned_to_refs' is checked. 
        """
        log_info(self,"return_scan() entering")
        
        # find the rotation direction: + or -
        rot_direction = 1
        if (self.parameters.end_pos - self.parameters.start_pos) < 0:
            rot_direction = -1
        
        # when scan range > 360, only take return images for the last 360 deg.
        if (self.parameters.end_pos - self.parameters.start_pos) * rot_direction > 360:
            limit_pos = self.parameters.end_pos - (360 * rot_direction)
        else:
            limit_pos = self.parameters.start_pos

        i=len(self.in_pars['nb_group_points'])-1
        
        rot_pos_list = list()
        rot_position = self.rotation_axis.position
        while (rot_position - limit_pos) * rot_direction >= 0:
            rot_pos_list.append(rot_position)
            
            if self.parameters.return_images_aligned_to_refs:
                shift_angle = self.tomo_scan.in_pars['scan_step_size']*self.in_pars['nb_group_points'][i]
                rot_position = rot_position - (shift_angle * rot_direction)
                i-=1
            else:
                rot_position = rot_position - (90.0 * rot_direction)
        
        #print (rot_pos_list)
        
        # camera parameters
        #lima_ctrl_params = self.saving.lima_saving_parameters()
        lima_ctrl_params = self.tomo_ccd.lima_saving_parameters(len(rot_pos_list))
        
        # set parameters if camera is in accumulation mode
        if self.tomo_ccd.det_proxy.acq_mode == "ACCUMULATION":
            if self.tomo_ccd.det_proxy.acc_nb_frames > 1:
                lima_ctrl_params['acc_max_expo_time'] = self.parameters.exposure_time / self.tomo_ccd.det_proxy.acc_nb_frames
            else:
                lima_ctrl_params['acc_max_expo_time'] = self.parameters.exposure_time
            
        # scan title
        title = "static images"
        
        # add image identification to the common image header
        header['image_key']     = '0'        # projection NXtomo definition
        
        # add meta data
        meta_data = self.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info['detector'] = meta_data['technique']['detector']
        scan_info = {'technique' : scan_info}
        
        # on error go back to initial position
        restore_list = (cleanup_axis.POS,)
        with error_cleanup(self.rotation_axis, restore_list=restore_list):
            
            # set-up the default chain with Lima parameters
            #settings={"device": self.tomo_ccd.detector,
                      #"controller_settings": lima_ctrl_params,
                     #}
            #DEFAULT_CHAIN.set_settings([settings]) 
            
            #deactivate default chain preset
            default_presets = DEFAULT_CHAIN._presets
            #DEFAULT_CHAIN._presets = {}
            
            # scan for static return images
            ret_scan = pointscan(self.rotation_axis, rot_pos_list, self.parameters.exposure_time, self.tomo_ccd.detector.image, 
                                 name=title, title=title, scan_info=scan_info, save=True, run=False)
            ret_scan.update_ctrl_params(self.tomo_ccd.detector,lima_ctrl_params)
            
            # add common header preset
            header_preset = CommonHeaderPreset(self.tomo_ccd, header)
            ret_scan.add_preset(header_preset)
            
            # add shutter preset
            shutter_preset = FastShutterPreset(self.shutter)
            ret_scan.add_preset(shutter_preset)
            
            # add to scan sequence when requested
            if scan_sequence != None:
                scan_sequence.add(ret_scan)
            
            # run the return scan
            ret_scan.run()
            
            #reactivate default chain preset
            DEFAULT_CHAIN._presets = default_presets
        
        log_info(self,"ret_scan() leaving")


    def dark_scan(self, dark_n=None, expo_time=None, shutter_preset=None, 
                  scan_sequence=None, header={}, save=True, run=True):
        """
        Acquires images with beam shutter closed.
        """
        log_info(self,"dark_scan() entering")
     
        if dark_n is None:
            dark_n = self.parameters.dark_n
        if expo_time is None:
            expo_time = self.parameters.exposure_time
        if shutter_preset is None:
            shutter_preset = DarkShutterPreset(self.shutter)
        
        # dark images to be taken?
        if self.parameters.dark_n > 0:
            # check rotation has stopped
            while self.rotation_axis.state == AxisState.MOVING:
                time.sleep (0.02)
        
        with capture_exceptions(raise_index=0) as capture:
            with capture():
            
                # Acquisition mode handling
                requested_acq_mode = "SINGLE"
                if self.parameters.dark_images_in_accumulation:
                    requested_acq_mode = "ACCUMULATION"
                    
                acq_mode = self.tomo_ccd.det_proxy.acq_mode
                if acq_mode != requested_acq_mode:
                    # set detector mode when necessary and re-apply all image parameters
                    self.tomo_ccd.set_acq_mode (requested_acq_mode)
                
                # prepare parameters
                nbframes  = dark_n
                lima_ctrl_params = self.tomo_ccd.lima_saving_parameters(nbframes)
                
                lima_acq_params = {}
                lima_acq_params['acq_mode'] = requested_acq_mode 
                
                if requested_acq_mode == "ACCUMULATION":
                    lima_acq_params['acc_max_expo_time'] = expo_time
                    expo_time = expo_time * dark_n
                    nbframes  = 1
                        
                # scan title
                title = "dark images"
                
                # add image identification to the common image header
                header['image_key']     = '2'    # dark field NXtomo definition
                
                # add meta data
                meta_data = self.meta_data.tomo_scan_info()
                scan_info = {}
                scan_info['dark'] = meta_data['technique']['dark']
                scan_info['detector'] = meta_data['technique']['detector']
                scan_info = {'technique' : scan_info}
                
                # scan for dark images
                dark_scan = self.tomo_ccd.image_scan(expo_time, nbframes, title, 
                                                     lima_acq_params=lima_acq_params,
                                                     lima_ctrl_params=lima_ctrl_params,
                                                     scan_info=scan_info, save=save, run=False)
                
                # add common header preset
                header_preset = CommonHeaderPreset(self.tomo_ccd, header)
                dark_scan.add_preset(header_preset)
                
                # add dark shutter preset
                dark_scan.add_preset(shutter_preset)
                
                # add to scan sequence when requested
                if scan_sequence != None:
                    scan_sequence.add(dark_scan)
                    self.list_dark_scans.append(dark_scan)
                
                # run the ref scan
                if run == True:
                    dark_scan.run()
            
                    # set back detector mode when necessary and re-apply all image parameters
                    if acq_mode != requested_acq_mode:
                        self.tomo_ccd.set_acq_mode (acq_mode)
                
                return dark_scan
            
            # test if an error has occured
            if len(capture.failed) > 0:
                log_error(self,'A problem occured during the dark scan, scan aborted')
                log_error(self, capture.exception_infos)
                log_error(self, "\n")
                
                # set back detector mode when necessary and re-apply all image parameters
                self.tomo_ccd.set_acq_mode (acq_mode)

                        
        log_info(self,"dark_scan() leaving")

    
    
    def ref_scan(self, ref_n=None, expo_time=None, projection=1, turn=0, shutter_preset=None, 
                 scan_sequence=None, header={}, save=True, run=True):
        """
        Acquires images with the sample out of the beam.
        """
        log_info(self,"ref_scan() entering")
        
        if ref_n is None:
            ref_n = self.parameters.ref_n
        if expo_time is None:
            expo_time = self.parameters.exposure_time
        if shutter_preset is None:
            shutter_preset = FastShutterPreset(self.shutter)
        
        # camera parameters
        lima_ctrl_params = self.tomo_ccd.lima_saving_parameters(self.parameters.ref_n)
        lima_acq_params = {}
            
        # scan title
        if turn > 0:
            title = "reference images " + str(projection) + "_" + str(turn)
        else:
            title = "reference images " + str(projection)
        
        # add image identification to the common image header
        header['image_key']     = '1'         # flat field NXtomo definition
        
        # add meta data
        meta_data = self.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info['reference'] = meta_data['technique']['reference']
        scan_info['reference']['projection'] = projection
        scan_info['detector'] = meta_data['technique']['detector']
        scan_info = {'technique' : scan_info}
        
        # on error go back to initial position
        restore_list = (cleanup_axis.POS,)
        with error_cleanup(*self.reference.ref_motors, restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
            
                    # scan for reference images
                    ref_scan = self.tomo_ccd.image_scan(expo_time, ref_n, title, 
                                                         lima_acq_params=lima_acq_params,
                                                         lima_ctrl_params=lima_ctrl_params,
                                                         scan_info=scan_info, save=save, run=False)
                    
                    # add shutter preset
                    ref_scan.add_preset(shutter_preset)
                    
                    # add common header preset
                    header_preset = CommonHeaderPreset(self.tomo_ccd, header)
                    ref_scan.add_preset(header_preset)
                    
                    # add reference preset
                    ref_mot_preset = ReferenceMotorPreset(self.reference)
                    ref_scan.add_preset(ref_mot_preset)
                                    
                    # add to scan sequence when requested
                    if scan_sequence != None:
                        scan_sequence.add(ref_scan)
                        self.list_ref_scans.append(ref_scan)
            
                    # run the ref scan
                    if run == True:
                        ref_scan.run()
                    
                    return ref_scan
            
                # test if an error has occured
                if len(capture.failed) > 0:
                    log_error(self,'A problem occured during the reference scan, scan aborted')
                    log_error(self, capture.exception_infos)
                    log_error(self, "\n")
            
        log_info(self,"ref_scan() leaving")



    def def_ref_disp(self):
        """
        Calculates from the camera field-of-view, the default lateral motor displacement needed
        to move sample out of the beam.
        For half acquisition, it determines the lateral motor displacement needed 
        to see half of the sample in the camera field-of-view.
        """
        ref_disp = self.tomo_ccd.field_of_view(self.optic.magnification)
        
        if self.parameters.half_acquisition:
            ref_disp *= self.halftomo.parameters.sample_size_in_fov_factor
        # add 10% and round to nearest 0.1 mm
        return int(11 * ref_disp + 0.5) / 10


    def update_reference_positions(self):
        """
        Updates in-beam and out-beam positions of reference motors.
        Takes the actual motor position for in-beam position.
        Default displacement is used for out-beam position except if 
        the user chooses to correct it.
        """

        for m in self.reference.ref_motors:
            mot_index = self.reference.ref_motors.index(m)
            in_beam_pos = self.reference.parameters.in_beam_position
            in_beam_pos[mot_index] = m.position

            self.reference.set_in_beam_position(in_beam_pos)

            out_beam_pos = self.reference.parameters.out_of_beam_displacement

            if mot_index == 0:
                calculated_pos = self.def_ref_disp()
                
                if calculated_pos > out_beam_pos[mot_index]:
                    out_beam_pos[mot_index] = calculated_pos
                    
                    print(f'Out beam displacement for {m.name} has been set to default calculated value: {calculated_pos}')
            
            self.reference.set_out_of_beam_displacement(out_beam_pos)


    def prepare(self):
        """
        Prepares tomo acquisition by:
            - configuring saving 
            - setting image flipping
            - updating reference motors positions
            - adapting lateral alignment motor position to half acquisition or full acquisition
            - moving rotation to start position
            - moving detector to the appropriate sample-detector distance
        """
        print ("Prepare tomo sequence")
        self.sequence_prepare = False
        
        # Verify the active detecor is still available
        self.tomo_ccd.verify_active_detector()
        
        # apply image flipping specified for the optic used
        self.tomo_ccd.set_image_flipping(self.optic.image_flipping)
        
        # set pixel rate to maximum
        if self.tomo_ccd.detector.camera_type.lower() == 'pco':
            self.tomo_ccd.set_max_pixel_rate()

        change = False
        if self.parameters.half_acquisition:
            self.active_tomo = self.halftomo
            if self.field_of_view == 'Full':
                change = True
                self.active_tomo.started_once = False
            else:
                self.active_tomo.started_once = True
            mot_pos = self.halftomo.parameters.acquisition_position
            self.field_of_view = 'Half'
        else:
            if self.field_of_view == 'Half':
                change = True
                print(f'Restoring {self.lateral_align_axis.name} full-frame pos. reference: {self.halftomo.parameters.full_frame_position}\n')
                mot_pos = self.halftomo.parameters.full_frame_position
            self.field_of_view  = 'Full'
            
        self.active_tomo.prepare()
        
        if change:
            print(f'New lat. alignment mot {self.lateral_align_axis.name} pos.')
            if mot_pos != self.lateral_align_axis.position:
                umv(self.lateral_align_axis,mot_pos)
            elif not self.parameters.half_acquisition:
                print(f'Motor {self.lateral_align_axis.name} already in full frame pos.\n')
            else:
                print('Warning: half acquisition pos is equal to full frame pos.\n')
        
        if not self.parameters.no_reference_groups or self.parameters.ref_images_at_start or self.parameters.ref_images_at_end: 
            self.update_reference_positions()
        
        if self.rotation_axis.position != self.parameters.start_pos:
            print(f'{self.rotation_axis.name} moving to start position')
            umv(self.rotation_axis,self.parameters.start_pos)
        
        if self.detector_axis.position != self.parameters.sample_detector_distance:
            print('Camera position is different from requested sample-detector distance.')
            print(f'Moving camera to {self.parameters.sample_detector_distance}')
            umv(self.detector_axis,self.parameters.sample_detector_distance)
        
        # sequence preset prepare, needs to execute after the tomo prepare!
        self.sequence_preset.prepare(self)
        
        self.sequence_prepare = True
        print('Sequence preparation ended well!\n')


    def run(self):
        
        self.prepare()
        self.show_scan_info()
        if self.sequence_prepare is False:
            return
        self.active_tomo.run()
        self.sequence  = 'tomo:basic'
        self.active_tomo = self.fasttomo
            
    
    def run_sequence(self, scan_sequence):
        """
        Runs tomo acquisition namely (according to selected options):
            - reference images
            - dark images
            - projection images
            - static images
        """
        print ("Run tomo sequence")
        scan_seq = scan_sequence
        
        scan_display = setup_globals.SCAN_DISPLAY.auto
        setup_globals.SCAN_DISPLAY.auto = True
        
        self.list_dark_scans = list()
        self.list_ref_scans = list()
        self.list_proj_scans = list()
        
        restore_list = (cleanup_axis.POS,)
        with error_cleanup(self.rotation_axis,self.tomo_ccd.detector, restore_list=restore_list):
            
            # do not return to initial position when no error is detected
            if self.parameters.return_to_start_pos == False:
                restore_list = list()
        
            with cleanup(self.rotation_axis,self.tomo_ccd.detector, restore_list=restore_list):
        
                scan_t0 = datetime.datetime.fromtimestamp(time.time())
        
                with capture_exceptions(raise_index=0) as capture:
                    with capture():
                        
                        log_info(self,"start scan sequence")
                        
                        # sequence preset start
                        self.sequence_preset.start()
                        
                        # take dark images
                        if self.parameters.dark_images_at_start:
                            self.dark_scan(scan_sequence=scan_seq)
                        
                        # Start and end positions of reference groups
                        group_start = self.in_pars['start_group_pos']
                        group_end = self.in_pars['start_group_pos'][1:]
                        group_end.append(self.parameters.end_pos)
                        
                        # loop over reference groups
                        for i in range(len(group_start)):
                            # take reference images
                            if i == 0:
                                if self.parameters.ref_images_at_start:
                                    self.ref_scan(projection=1, scan_sequence=scan_seq)
                            else:
                                self.ref_scan(projection=(self.in_pars['nb_group_points'][i-1]*i), scan_sequence=scan_seq)
                                
                            # projection scan title
                            if i == 0:
                                title = "projections " + str(1) + " - " + str(self.in_pars['nb_group_points'][i])
                            else:
                                title = "projections " + str(self.in_pars['nb_group_points'][i-1]*i+1) + " - " + str(self.in_pars['nb_group_points'][i-1]*i+self.in_pars['nb_group_points'][i])
        
                            
                            # Reference group projections
                            self.active_tomo.projection_scan(group_start[i], group_end[i], self.in_pars['nb_group_points'][i], 
                                                             title, scan_sequence=scan_seq)
                            
                        if not self.parameters.no_return_images:
                            # move back to end position
                            umv (self.rotation_axis, self.parameters.end_pos)

                            self.return_scan(scan_sequence=scan_seq)

                        if self.parameters.dark_images_at_end:
                            self.dark_scan(scan_sequence=scan_seq)
                                
                        if self.parameters.ref_images_at_end:
                            self.ref_scan(projection=(self.in_pars['nb_group_points'][i-1]*i+self.in_pars['nb_group_points'][i]), scan_sequence=scan_seq)
                            
                        # sequence preset stop
                        self.sequence_preset.stop()
                        
                        setup_globals.SCAN_DISPLAY.auto = scan_display

                                
                    if len(capture.failed) > 0:
                        print('\n')
                        log_error(self,f"A problem occured during {self.sequence.split(':')[-1]} sequence, sequence aborted")
                        log_error(self, capture.exception_infos)
                        log_error(self, "\n")
                        
                        # sequence preset stop
                        self.sequence_preset.stop()
                        
                        setup_globals.SCAN_DISPLAY.auto = scan_display
                        
                        self.sequence  = 'tomo:basic'
                        self.active_tomo = self.fasttomo
                        
                    else:
                        scan_tend = datetime.datetime.fromtimestamp(time.time()) 
                        print("Total scan sequence took {0}".format(scan_tend - scan_t0))
                        print(f"\n{self.sequence.split(':')[-1]} sequence ended well!\n")

        log_info(self,"sequence ended")
        

    def show_scan_info(self):
        print("\n=================================================================")
        print(f"Image size = {self.tomo_ccd.in_pars['image_mbsize']:.2f} MB")
        print(f"Scan start position: {self.parameters.start_pos}")
        print(f"Scan end position: {self.parameters.end_pos}")
        print(f"Scan number of images: {self.parameters.tomo_n}")
        print(f"Scan step size: {self.tomo_scan.in_pars['scan_step_size']:.3f} degrees")
        print(f"Scan point time: {self.tomo_scan.in_pars['scan_point_time']:.3f} sec (Exposure time: {self.parameters.exposure_time} sec)")
        print(f"Scan speed: {self.tomo_scan.in_pars['scan_speed']:.2f} degrees / sec")
        print(f'Saving path: {current_session.scan_saving.get_path()}')
        print(f'Image file format: {self.tomo_ccd.detector.saving.file_format}')
        print(f'Frames per file: {self.tomo_ccd.detector.saving.frames_per_file}')
        print(f"Estimated data rate ==> {round(self.tomo_ccd.in_pars['image_mbsize']/self.tomo_scan.in_pars['scan_point_time'],1)} MB/s")
        print(f"Approximative Estimated Scan Time: {self.in_pars['estimated_time_scan']}") 
        print("=================================================================")


    def topo_tomo(self, topo_start_pos, topo_end_pos, topo_nb_points, 
                   tomo_start_pos=None, tomo_end_pos=None, tomo_nb_points=None, run=True):
        """
        Prepares and runs topotomo acquisition.
        A nested scan is performed at each projection angle.
        Transmitted parameters are related to nested scan.
        """
        self.sequence = 'tomo:topo'
        
        if tomo_start_pos != None:
            self.parameters.start_pos = tomo_start_pos
        if tomo_end_pos != None:
            self.parameters.end_pos = tomo_end_pos
        if tomo_nb_points != None:
            self.parameters.tomo_n = tomo_nb_points

        topotomo = tomo.TopoTomo(self, topo_start_pos, topo_end_pos, topo_nb_points)
        
        self.active_tomo = topotomo

        if not run:
            self.prepare()
        else:    
            self.run()


    def mtomo (self, nb_turns, start_pos=None, start_turn=1):
        """
        Prepares and runs multiple full turn scans.
        Rotation axis is stopped and restarted at each turn.
        """
        self.sequence = 'tomo:multiturn'

        mtomo = tomo.Mtomo(self)
        
        self.active_tomo = self.fasttomo
        
        mtomo.prepare(nb_turns, start_pos, start_turn)

        mtomo.run()



    def progressive_tomo (self, nb_turns, start_pos=None, start_turn=1):
        """
        Prepares and runs progressive tomo.
        Multiple full turns are performed without stopping the rotation.
        """
        self.sequence = 'tomo:progressive'
        
        progtomo = tomo.Progressive(self)
        
        self.active_tomo = self.fasttomo
        
        progtomo.prepare(nb_turns, start_pos, start_turn)

        progtomo.run()
        
        

    def zseries (self, delta_pos, nb_scans, start_pos=None, start_nb=1, sleep=0.0):
        """
        Prepares and runs z_series acquisition.
        Multiple half or full turn scans are performed while z axis is moving at each turn 
        with a step defined by delta_pos
        Parameter sleep allows to wait some time between each scan
        """
        self.sequence = 'tomo:zseries'
        
        zseries = tomo.ZSeries(self)
        
        self.active_tomo = self.fasttomo
        
        zseries.prepare(delta_pos, nb_scans, start_pos, start_nb, sleep)

        zseries.run()


    def basic_scan (self, start_pos, end_pos, tomo_n, expo_time, run=True):
        """
        Prepares and runs a scan of tomo_n projections from start_pos to end_pos with an
        exposure time defined by expo_time
        """
        self.sequence = 'tomo:basic'

        self.parameters.start_pos     = start_pos
        self.parameters.end_pos       = end_pos
        self.parameters.tomo_n        = tomo_n
        self.parameters.exposure_time = expo_time

        #if self.parameters.to_dict() != self.last_parameters:
        self.active_tomo = self.fasttomo
        
        if not run:
            self.prepare()
        else:    
            self.run()

    def half_turn_scan (self, start_pos=None, run=True):
        """
        Prepares and runs a tomo of 180 degrees starting from start_pos
        """
        self.sequence = 'tomo:halfturn'
            
        if start_pos!= None:
            self.parameters.start_pos = start_pos
        self.parameters.end_pos       = self.parameters.start_pos + 180

        #if self.parameters.to_dict() != self.last_parameters:
        self.active_tomo = self.fasttomo
        
        if not run:
            self.prepare()
        else:    
            self.run()
        
    def full_turn_scan (self, start_pos=None, run=True):
        """
        Prepares and runs a tomo of 360 degrees starting from start_pos
        """
        self.sequence = 'tomo:fullturn'
            
        if start_pos!= None:
            self.parameters.start_pos = start_pos
        self.parameters.end_pos       = self.parameters.start_pos + 360

        #if self.parameters.to_dict() != self.last_parameters:
        self.active_tomo = self.fasttomo
        
        if not run:
            self.prepare()
        else:    
            self.run()
        
    def get_projection_scan(self):
        try:
            if len(self.list_proj_scans) == 1:
                return self.list_proj_scans[0]
            else:
                return self.list_proj_scans
        except AttributeError:
            raise Exception("Error no tomo sequence has been launched")
    
    def get_reference_scan(self):
        if self.parameters.ref_images_at_start or self.parameters.ref_images_at_end or not self.parameters.no_reference_groups:
            try:
                if len(self.list_ref_scans) == 1:
                    return self.list_ref_scans[0]
                else:
                    return self.list_ref_scans
            except AttributeError:
                raise Exception("Error no tomo sequence has been launched")
        else:
            raise Exception('No reference images has been requested, please change scan option in setup')
    
    def get_dark_scan(self):
        if self.parameters.dark_images_at_start or self.parameters.dark_images_at_end:
            try:
                if len(self.list_dark_scans) == 1:
                    return self.list_dark_scans[0]
                else:
                    return self.list_dark_scans
            except AttributeError:
                raise Exception("Error no tomo sequence has been launched")
        else:
            raise Exception('No dark images has been requested, please change scan option in setup')

    def show_config(self, all=True):
        """
        Allows to see parameters of all objects related to tomo
        """
        log_info(self,"show_config() entering")
        
        # Show the scan paramters
        print ("\nUsage: %s.parameters\n" % self.tomo_name)
        super().show_config()
        
        # if all is requested, show all other parameter sets
        if all == True:
            print ("\nUsage: %s.tomo_ccd.parameters\n" % self.tomo_name)
            self.tomo_ccd.show_config()
            print ("\nUsage: %s.optic.parameters\n" % self.tomo_name)
            self.optic.show_config()
            print ("Usage: %s.shutter.parameters\n" % self.tomo_name)
            self.shutter.show_config()
            print ("\nUsage: %s.reference.parameters\n" % self.tomo_name)
            self.reference.show_config()
            print ("Usage: %s.saving.parameters\n" % self.tomo_name)
            self.saving.show_config()

        log_info(self,"show_config() leaving")
        
        
    def save_scan_config(self, directory, all=True):
        """
        Allows to save in a specified directory:
            - scan parameters 
            - reference, optic, shutter and saving parameters if all is set to True
        """
        # Save the scan parameters
        super().save_scan_config(directory)
        
        # if all is requested, save all other parameter sets
        if all == True:
            self.tomo_ccd.save_scan_config(directory)
            self.optic.save_scan_config(directory)
            self.shutter.save_scan_config(directory)
            self.reference.save_scan_config(directory)
            self.saving.save_scan_config(directory)
            
            
    def load_scan_config(self, directory, all=True):
        """
        Allows to load from a specified directory:
            - scan parameters 
            - reference, optic, shutter and saving parameters if all is set to True
        """
        # Load the scan paramters
        super().load_scan_config(directory)
        
        # if all is requested, load all other parameter sets
        if all == True:
            self.tomo_ccd.load_scan_config(directory)
            self.optic.load_scan_config(directory)
            self.shutter.load_scan_config(directory)
            self.reference.load_scan_config(directory)
            self.saving.load_scan_config(directory)


    def reset(self, all=False):
        """
        Allows to remove from redis and set to default values:
            - scan parameters
            - reference, optic, shutter and saving parameters if all is set to True
        """
        log_info(self,"reset() entering")

        # Remove the scan paramters
        super().reset()
        
        # if all is requested, remove all other parameter sets
        if all == True:
            self.tomo_ccd.reset()
            self.reference.reset()
            self.optic.reset()
            self.shutter.reset()
            self.saving.reset()

        log_info(self,"reset() leaving")
