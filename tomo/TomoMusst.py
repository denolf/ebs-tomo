from bliss import setup_globals,global_map
from bliss.scanning.acquisition.musst import *
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.common import session
from bliss.common.logtools import log_info

import tomo

from enum import Enum
class TriggerType(Enum):
    POSITION  = 0
    TIME = 1

class TomoMusst:
    """
    Class to handle configuration of musst acquisition device 
    
    One method corresponds to one configuration 
    
    Musst configuration depends on scan type
    """

    def __init__(self, tomo_name, card, motor, *args, **kwargs):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        card : musst controller
            musst device object
        motor : axis controller
            motor with encoder that will trigger musst
        enc_channel : musst channel linked to motor encoder
        mot_chan : musst channel id
        trigger : enum
            musst trigger mode (position : 0 / time : 1)
        storelist : list
            name of variables/data recorded by musst device during tomo 
        """

        self.name = tomo_name+".musst"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        self.tomo_name = tomo_name

        self.card = card
        print(card)

        self.motor = motor
        # get musst channel associated to motor
        try:
            self.enc_channel = self.card.get_channel_by_name(self.motor.name)
            self.mot_chan = self.enc_channel.channel_id
        except RuntimeError:
            self.enc_channel = None
            self.mot_chan = None
            print("No musst channel associated to this motor! \
            \nOnly step scans or continuous scans with soft synchronization are possible!")

        self.storelist = ["timer_raw",f"{self.motor.name}_raw"]

        log_info(self,"__init__() leaving")
        
    def sync_motor(self):
        
        self.motor.wait_move()
        self.enc_channel.value = int(round((self.motor.position*self.motor.steps_per_unit+0.5)//1))

    def prepare_continuous(self, start_pos, exposure_time, latency_time, nb_points, scan_step_size, scan_point_time, nb_trigs=1, trigger_type=TriggerType.TIME):
        """
        Configures MusstAcquisitionSlave Bliss object 
        Used by continuous scan synchronized by musst
        """

        log_info(self,"prepare_continuous() entering")

        # mot_zero is a list of encoder values corresponding to 
        # start position tomo parameter
        mot_start = int(round((start_pos * self.motor.steps_per_unit+0.5)//1))

        trig_delta = 0
        if nb_trigs > 1:
            # convert exposure time to timer ticks
            trig_delta = (scan_point_time - latency_time) / nb_trigs 
        
        gatewidth = int(round(((((nb_trigs - 1) * trig_delta + exposure_time / nb_trigs) * self.card.get_timer_factor())+0.5)//1))
        trig_delta = int(round(trig_delta * self.card.get_timer_factor() +0.5)//1)

        # --- position mode
        if trigger_type == TriggerType.POSITION:
            pos_delta = int(scan_step_size * self.motor.steps_per_unit)
            
            # musst will correct step size at each image
            enc_delta = int(pos_delta)
            mot_error = abs(int((enc_delta - pos_delta) * 2**31))

            # configure musst variables used by 'ftomo_acc' program 
            # corresponding to a reference group 
            musst_vars = {
                              "MOTSTART" :   mot_start,
                              "NPTS"   :    int(nb_points),
                              "PTMODE" :    1,
                              "PTDELTA" :   pos_delta,
                              "GATEWIDTH"   :   gatewidth,
                              "MOTERR"  :   mot_error,
                              "NTRIGS"  :   int(nb_trigs),
                              "TRIGDELTA"   :   trig_delta
                             }

        # --- time mode
        if trigger_type == TriggerType.TIME:
            time_delta = int(scan_point_time*self.card.get_timer_factor())


            musst_vars = {
                              "MOTSTART" :   mot_start,
                              "NPTS"   :    int(nb_points),
                              "PTMODE" :    0,
                              "PTDELTA" :   time_delta,
                              "GATEWIDTH"   :   gatewidth,
                              "NTRIGS"  :   int(nb_trigs),
                              "TRIGDELTA"   :   trig_delta
                             }
        
        # allows to replace alias 'MOTOR_CHANNEL' in 'ftomo_acc' program with channel associated to motor 'CH#'
        template_replacement= { "$MOTOR_CHANNEL$": "CH%d"%self.mot_chan}
        
        # MUSST acquisition master
        musst_master = MusstAcquisitionMaster(self.card,
                                          program = 'ftomo_acc.mprg',
                                          program_start_name = 'TOMO',
                                          program_abort_name = 'TOMO_CLEAN',
                                          program_template_replacement=template_replacement,
                                          vars=musst_vars)
        
        # MUSST acquisition slave                                  
        musst_slave = MusstAcquisitionSlave(self.card, store_list=self.storelist)    # store encoder and timer data

        log_info(self,"prepare_continuous() leaving")

        return musst_master, musst_slave
        

    def prepare_sweep(self, start_pos, exposure_time, nb_trig, scan_step_size, undershoot):
        """
        Configures MusstAcquisitionSlave Bliss object 
        Used by continuous scan without gap
        """

        log_info(self,"prepare_sweep() entering")

        # mot_zero is a list of encoder values corresponding to 
        # start position tomo parameter
        mot_zero = int(round((start_pos * self.motor.steps_per_unit+0.5)//1))

        pos_delta = scan_step_size * self.motor.steps_per_unit
        # musst will correct step size at each image
        enc_delta = int(pos_delta)
        mot_error = abs(int((enc_delta - pos_delta) * 2**31))

        # configure musst variables used by 'ftomo_wo_gap' program 
        # corresponding to a reference group
        first_iter_vars = {"POSSTART"   : mot_zero,
                           "POSDELTA"   : enc_delta,
                           "NPULSES"    : nb_trig,
                           "MOTERR"     : mot_error,
                           "UNDERSHOOT" : int(undershoot * self.motor.steps_per_unit * 0.9)    # target value used for motor back movement to update target register  
        }
        musst_vars = first_iter_vars
        
        # allows to replace alias 'MOTOR_CHANNEL' in 'ftomo_wo_gap' program with channel associated to motor 'CH#'
        template_replacement= { "$MOTOR_CHANNEL$": "CH%d"%self.mot_chan}
        
        # MUSST acquisition master
        musst_master = MusstAcquisitionMaster(self.card,
                                              program = 'ftomo_wo_gap.mprg',
                                              program_start_name = 'SWEEP',
                                              program_abort_name = 'SWEEPCLEAN',
                                              program_template_replacement=template_replacement,
                                              vars=musst_vars)
        
        # MUSST acquisition slave                                  
        musst_slave = MusstAcquisitionSlave(self.card, store_list=self.storelist)    # store encoder and timer data
        
        log_info(self,"prepare_sweep() leaving")
        return musst_master, musst_slave


        

    def prepare_soft(self, exposure_time):
        """
        Configures SoftTriggerMusstAcquisitionMaster object
        Used by step scan or continuous scan synchronized by soft 
        """

        log_info(self,"prepare_soft() entering")

        # will run RUNCT command 
        musst_acq = tomo.SoftTriggerMusstAcquisitionMaster(self.card, exposure_time)

        log_info(self,"prepare_soft() leaving")

        return musst_acq
    
    
    
    def prepare_calculation(self,musst_acq,data_per_point=1,data_per_line=None):
        """
        Configures calculation devices
        Used to convert encoder and timer data from the musst in degrees and seconds  
        """
        
        log_info(self,"prepare_calculation() entering")
        
        # use musst timer factor to convert timer musst data into seconds
        #data_time = tomo.CScanMusstChanCalc('time','TIMER', self.card.get_timer_factor(), 'trigger')
        data_time = tomo.MusstConvDataCalc('pstn',self.card,'timer_raw', data_per_point, data_per_line, self.card.get_timer_factor(), 'elapsed_time')
        calc_time_device = CalcChannelAcquisitionSlave('calc_time_device',(musst_acq,),data_time,data_time.acquisition_channels)
        
        # use motor steps per unit to convert encoder musst data into degrees 
        data_pos = tomo.MusstConvDataCalc('calc',self.card,f'{self.motor.name}_raw', data_per_point, data_per_line, self.motor.steps_per_unit, self.motor.name)
        calc_pos_device = CalcChannelAcquisitionSlave('calc_pos_device',(musst_acq,),data_pos,data_pos.acquisition_channels)
        
        calc_device = [calc_time_device, calc_pos_device]
        
        log_info(self,"prepare_calculation() leaving")
        
        return calc_device            
