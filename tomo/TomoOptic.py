import sys
import gevent
import numpy as np
import time

import PyTango

import bliss
from bliss import global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

from tomo.TomoParameters import TomoParameters

class TomoOptic(TomoParameters):
    """
    Base class for all tomo optic objects.
    The class implements all the standard methods to be implemented for every optic.
    
    **Attributes**:
    
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    rotc_motor : Bliss Axis object
        The camera rotation motor for the ojective
    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The scan range for the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. 
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    """
    
    
    def __init__(self, name, config, param_name=None, param_defaults=None):
        # init logging
        self.log_name = name+".optic"
        global_map.register(self, tag=self.log_name)
        log_info(self,"__init__() entering")
        
        self.__name = name
        self.__config = config
        
        # Initialise the TomoParameters class
        super().__init__(param_name, param_defaults)
        
        
        try:
            self.__image_flipping_hor  = config["image_flipping_hor"]
        except:
            self.__image_flipping_hor  = False
        try:
            self.__image_flipping_vert = config["image_flipping_vert"]
        except:
            self.__image_flipping_vert = False
        
        try:
            self.__rotc_mot = config["rotc_motor"]
        except:
            self.__rotc_mot = None
        
        try:
            self.__focus_mot = config["focus_motor"]
        except:
            self.__focus_mot = None
            
        try:
            self.__focus_type = config["focus_type"]
        except:
            self.__focus_type = "unknown"
        try:
            self.__focus_scan_range = config["focus_scan_range"]
        except:
            self.__focus_scan_range = 0
        try:
            self.__focus_scan_steps = config["focus_scan_steps"]
        except:
            self.__focus_scan_steps = 0
        try:
            self.__focus_lim_pos    = config["focus_lim_pos"]
        except:
            self.__focus_lim_pos    =  0
        try:
            self.__focus_lim_neg    = config["focus_lim_neg"]
        except:
           self.__focus_lim_neg    = 0
                
        log_info(self,"__init__() leaving")

    @property
    def name(self):
        """
        The name of the optic
        """
        return self.__name
        
    @property
    def config(self):
        """
        The configuration of the optic
        """
        return self.__config
    
    
    @property
    def description(self):
        """
        The name string of the current optic.
        Musst be implemented for every optic.
        """
        pass
        
    @property
    def type(self):
        """
        The class name of the optic.
        Musst be implemented for every optic.
        """
        pass
    
    @property
    def magnification(self):
        """
        Returns the magnification of the current optic used.
        Musst be implemented for every optic.
        """
        pass
        
    @property    
    def image_flipping(self):
        """
        Returns the implied horizontal and vertical image flipping as a list
        """
        return [self.__image_flipping_hor, self.__image_flipping_vert]
        
    def setup(self):
        """
        Set-up the optic
        Musst be implemented for every optic
        """
        pass
    
    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an optics cannot be determined, the reason gets printed.
        Musst be implemented for every optic
        """
        pass
        
    @property
    def objective(self):
        """
        Reads and sets the current objective.
        Must be implemented for optics with more than one objective.
        """
        return 1
    
    @objective.setter
    def objective(self, value):
        """
        Moves to an objective
        Must be implemented for optics with more than one objective.
        """
        pass
        
    def rotc_motor (self):
        """
        Returns the Bliss Axis object of the rotation motor to be used for the current objective
        """
        
        return self.__rotc_mot

    def focus_motor (self):
        """
        Returns the Bliss Axis object of the focus motor to be used for the current optic
        """
        return self.__focus_mot
        
    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        if self.__focus_mot == None:
            raise ValueError ("No focus motor defined for the optic!")
            
        scan_params = {}
        scan_params['focus_type'] = self.__focus_type
        scan_params['focus_scan_range'] = self.__focus_scan_range
        scan_params['focus_scan_steps'] = self.__focus_scan_steps
        scan_params['focus_lim_pos'] = self.__focus_lim_pos
        scan_params['focus_lim_neg'] = self.__focus_lim_neg
    
        return scan_params
        
    def __info__(self):
        info_str  = f"{self.name} optic info:\n"
        info_str += f"  description   = {self.description}\n"
        info_str += f"  objective     = {self.objective} \n"
        info_str += f"  magnification = {self.magnification} \n"
        if self.rotc_motor() == None:
            info_str += f"  rotc motor    = None \n"
        else:
            info_str += f"  rotc motor    = {self.rotc_motor().name} \n"
        if self.focus_motor() == None:
            info_str += f"  focus motor   = None \n"
        else:
            info_str += f"  focus motor   = {self.focus_motor().name} \n"
        info_str += f"  focus scan    = {self.focus_scan_parameters()} \n"
        
        return info_str
