import sys
import gevent
import numpy as np
import time



from bliss.common.standard import *
from bliss.common.standard import info
from bliss.common.logtools import log_info,log_debug
from bliss.config.settings import ParametersWardrobe

class TomoParameters:
    """
    Base class for all tomo objects to handle persistant parameters.
    A parameter set is stored in Redis under the specified name during initialization.
    It can be initialised to a set of default parameters with default values.
    The default values musst be passed as a dictionary in the form {"paramter_name" : default_value, ...}
    """
    
    def __init__(self, param_name=None, param_defaults=None):
        """
        Reads the parameter set from Redis and add the default values when necessary
        """
        self.param_name     = param_name
        
        if self.param_name != None:
            self.parameters = ParametersWardrobe(self.param_name)
            
            if param_defaults != None:
                parameters_dict = self.parameters.to_dict(export_properties=True)
                for k,v in param_defaults.items():
                    if k not in parameters_dict:
                        self.parameters.add(k,v)

        
    def save_scan_config(self, directory):
        """
        Allows to save in a file a parameter set 
        """
        if self.param_name != None:
            file_name = directory + "/" + self.param_name
            self.parameters.to_file(file_name)
        
        
    def load_scan_config(self, directory):
        """
        Allows to load from a file a parameter set 
        """
        if self.param_name != None:
            file_name = directory + "/" + self.param_name
            self.parameters.from_file(file_name, "default")
        
        
    def show_config(self):
        """
        Prints the parameters of the object
        """
    
        if self.param_name != None:
            print(info(self.parameters))  
        
        
    def reset(self):
        """
        Deletes all parameters of the object from Redis 
        to re-start with the default values
        """
        
        if self.param_name != None:
            #
            # remove all parameters one by one.
            # Cannot remove all parameters with the name given at object creation!
            # self.parameters.remove(self.tomo_name+':saving_parameters')
            #
            parameters_dict = self.parameters.to_dict()
            for k in parameters_dict.items():
                self.parameters.remove("."+k[0])
            
