import sys
import gevent
import numpy as np
import time

from bliss import global_map
from bliss.common.logtools import log_info,log_debug
from bliss.common import session
from bliss.common.motor_group import Group
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog

from tomo.TomoParameters import TomoParameters

class TomoRefMot(TomoParameters):
    def __init__(self, tomo_name, tomo_config, tomo, *args, **kwargs):
        # init logging
        self.name = tomo_name+".reference"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        config = tomo_config["reference"]
        self.tomo_name = tomo_name
        self.tomo      = tomo
       
        self.ref_motors  = config["motors"]
        self.settle_time = config["settle_time"]
        self.power_onoff = config.get("power_onoff", False)
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':reference_parameters'
        ref_defaults = {}
        ref_defaults['in_beam_position']         = [0.0]
        ref_defaults['out_of_beam_displacement'] = [0.0]
        
        # Initialise the TomoParameters class
        super().__init__(param_name, ref_defaults)
        
        parameters_dict = self.parameters.to_dict()
        for k,v in ref_defaults.items():
            if k == 'in_beam_position' or k == 'out_of_beam_displacement':
                if len(parameters_dict[k]) != len(self.ref_motors):
                    parameters_dict[k] = [0.0] * len(self.ref_motors)
                    self.parameters.from_dict(parameters_dict)
        
        #
        # test the initialisation of the motors
        #
        for m in self.ref_motors:
            print ("Reference axis used: %s" % m.name)
            print ("Reference axis position: %f" % m.position)
            
        #
        # Create the motor group
        #
        self.ref_mot_group = Group(*self.ref_motors)
        
        log_info(self,"__init__() leaving")

    
    def setup(self):
        """
        Set-up reference motors displacement
        """
        def_ref_disp = self.tomo.def_ref_disp()
        dlg_ref_disp = UserMsg(label=(f"The default out of beam displacement for motor {self.ref_motors[0].name} is: " + str(def_ref_disp)))
        dlg_out_disp_mot1 = UserFloatInput(label=f"Out of beam displacement for motor {self.ref_motors[0].name}: ", defval=self.parameters.out_of_beam_displacement[0])

        if len(self.ref_motors) > 1:
            list_dlg_out_disp_mot = []
            i=0
            for m in self.ref_motors[1:]:
                i+=1
                dlg_out_disp_mot = UserFloatInput(label=f"Out of beam displacement for motor {m.name}: ", defval=self.parameters.out_of_beam_displacement[i])
                list_dlg_out_disp_mot.append(dlg_out_disp_mot)
            
            ct = Container( [dlg_ref_disp,dlg_out_disp_mot1,*list_dlg_out_disp_mot], title="")
        else:
            ct = Container( [dlg_ref_disp,dlg_out_disp_mot1], title="")
        
        ret = BlissDialog( [ [ct] ], title='Reference Displacement Setup').show()
        
        if ret != False:
            out_disp = self.parameters.out_of_beam_displacement
            out_disp[0] = ret[dlg_out_disp_mot1]
            if len(self.ref_motors) > 1:
                i=0
                for dlg_out_disp in list_dlg_out_disp_mot:
                    i+=1
                    out_disp[i] = ret[dlg_out_disp]
                    
            self.set_out_of_beam_displacement(out_disp)
        
            i=0
            for m in self.ref_motors: 
                print(f'Out of beam displacement for reference motor {m.name}: ' + str(self.parameters.out_of_beam_displacement[i]) + ' mm')
                i+=1   

    
    def __info__(self):
        info_str  = f"Reference displacement info:\n"
        info_str += f"  motors                   = {[m.name for m in self.ref_motors]}\n"
        info_str += f"  in_beam_position         = {self.parameters.in_beam_position} \n"
        info_str += f"  out_of_beam_displacement = {self.parameters.out_of_beam_displacement} \n"
        info_str += f"  settle_time              = {self.settle_time} \n"
        info_str += f"  power_onoff              = {self.power_onoff} \n"
        return info_str
        
        
    def move_in(self):
        """
        Moves sample to in-beam position
        """
        log_info(self,"move_in() entering")
        
        motion = []

        in_beam_position = self.parameters.in_beam_position
        
        #
        # check coherence
        #
        if len(self.ref_motors) != len(in_beam_position):
            raise ValueError ("Number of in-beam positions does not correspond to the number\
                              of reference motors!")
        
        for i in range(len(self.ref_motors)):
            motion.append(self.ref_motors[i])
            motion.append(in_beam_position[i])
        
        
        #
        # move all reference motors in parallel
        #
        self.ref_mot_group.move(*motion)
        
        #
        # power off the motors
        #
        if self.power_onoff is True:
            for m in self.ref_motors:
                m.off()
                ## need to add those lines: bug with power off
                #self.ref_mot_group.stop()
                #m.sync_hard()
                #while 'SCSTOP' not in self.ref_mot_group.state:
                    #self.ref_mot_group.stop()
                    #m.sync_hard()
        
        #
        # wait for motor stabilization
        #
        time.sleep(self.settle_time)
        
        log_info(self,"move_in() leaving")
        
            
        
    def move_out(self):
        """
        Moves sample to out-beam position
        """
        log_info(self,"move_out() entering")
        
        motion = []
        
        in_beam_position = self.parameters.in_beam_position
        out_of_beam_displacement = self.parameters.out_of_beam_displacement
        
        #
        # check coherence
        #
        if len(self.ref_motors) != len(in_beam_position):
            raise ValueError ("Number of in-beam positions does not correspond to the number\
                              of reference motors!")  
        if len(self.ref_motors) != len(out_of_beam_displacement):
            raise ValueError ("Number of displacement positions does not correspond to the number\
                              of reference motors!")
                              
        #
        # calculate out of beam positions
        #                      
        for i in range(len(self.ref_motors)):
            motion.append(self.ref_motors[i])
            motion.append(in_beam_position[i] + out_of_beam_displacement[i])
        
        log_debug(self,motion)
        
        #
        # power on the motors
        #
        if self.power_onoff is True:
            for m in self.ref_motors:
                m.on()
            
        #
        # move all reference motors in parallel
        #
        self.ref_mot_group.move(*motion)
      
        #
        # wait for motor stabilization
        #
        time.sleep(self.settle_time)
        
        log_info(self,"move_out() leaving")
        
        
    def set_in_beam_position(self, positions):
        """
        Defines in-beam position
        """
        log_info(self,"set_in_beam_position() entering")
        
        if len(self.ref_motors) == len(positions):
            self.parameters.in_beam_position = positions
        else:
            raise ValueError ("Number of positions does not correspond to the number\
                              of reference motors")
                              
        log_info(self,"set_in_beam_position() leaving")
        
        
        
    def set_out_of_beam_displacement(self, distances):
        """
        Defines out-beam position
        """
        log_info(self,"set_out_of_beam_displacement() entering")
        
        
        if len(self.ref_motors) == len(distances):
            self.parameters.out_of_beam_displacement = distances
        else:
            raise ValueError ("Number of positions does not correspond to the number\
                              of reference motors")
    
        log_info(self,"set_out_of_beam_displacement() leaving")
    
