import sys
import gevent
import numpy as np
import os

from bliss import global_map, setup_globals, current_session
from bliss.common.logtools import log_info,log_debug

from bliss.scanning.scan import ScanSaving
from bliss.setup_globals import *

from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog

from tomo.TomoParameters import TomoParameters


class TomoSaving(TomoParameters):
    def __init__(self, tomo_name, tomo_config, tomo_ccd, *args, **kwargs):
        # init logging
        self.name = tomo_name+".saving"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        
        self.tomo_name = tomo_name
        self.tomo_ccd  = tomo_ccd
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':saving_parameters'
        saving_defaults = {}
        saving_defaults['lbs_active'] = False
        saving_defaults['comment']    = ''
        
        # Initialise the TomoParameters class
        super().__init__(param_name, saving_defaults)
  
        log_info(self,"__init__() leaving")
    
    
    def setup(self):
        """
        Set-up scan saving parameters
        """
        scan_saving_config = current_session.scan_saving.scan_saving_config
        scan_saving        = current_session.scan_saving
        
        values = [("visitor","visitor"),("inhouse","inhouse"),("tmp","tmp")]
        proposal_type = scan_saving.proposal_type
        # get the current proposal type as default value
        index = 0
        for i in values:
            if proposal_type == i[0]:
                type_index = index
                break
            else:
                index = index + 1
        
        dlg_disk = UserChoice(values=values, defval=type_index)
        dlg_lbs = UserCheckBox(label="Save on lbs", defval=self.parameters.lbs_active)
        ct1 = Container( [dlg_disk], title="Storage Space")
        ct2 = Container( [dlg_lbs], title="LBS option")
        ret = BlissDialog( [ [ct1], [ct2] ] , title='Saving Setup').show()
        

        # returns False on cancel
        if ret != False:
            proposal_type = ret[dlg_disk]
            use_lbs       = ret[dlg_lbs]
            
            if proposal_type == 'visitor':
                base_path = scan_saving_config["visitor_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(user experiment)'
            elif proposal_type == 'inhouse':
                base_path = scan_saving_config["inhouse_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(Empty string for default proposal name or must start with blc or ih)'
            elif proposal_type == 'tmp':
                base_path = scan_saving_config["tmp_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(Musst start with test, temp or tmp)'
            
            current_proposal = scan_saving.proposal
            current_sample   = scan_saving.sample
            current_dataset  = scan_saving.dataset
            current_comment  = self.parameters.comment
            
            # if the base path has changed, clean-up proposal, sample and dataset
            if scan_saving.base_path != base_path:
                current_proposal =''
                current_sample   =''
                current_dataset  =''
                current_comment  =''
            
            dlg_path      = UserMsg(label=f"Base Path {base_path}")
            v_proposal    = Validator(self.proposal_validator, proposal_type)
            dlg_proposal  = UserInput(label="Proposal Name ", defval=current_proposal, validator=v_proposal)
            dlg_tips      = UserMsg(label=proposal_tips)
            dlg_sample    = UserInput(label="Sample Name", defval=current_sample)
            dlg_dataset   = UserInput(label="Dataset Name", defval=current_dataset)
            dlg_comment   = UserInput(label="Comment", defval=current_comment)
            
            ret = BlissDialog( [ [dlg_path], [dlg_proposal], [dlg_tips], [dlg_sample], [dlg_dataset], [dlg_comment]] , title='Saving Setup').show()
            
            # returns False on cancel
            if ret != False:
                # get main parameters
                new_proposal = ret[dlg_proposal]
                new_sample = ret[dlg_sample]
                new_dataset = ret[dlg_dataset]
                new_comment = ret[dlg_comment]
        
                self.configure (proposal_type, new_proposal, new_sample, new_dataset, 
                                new_comment, use_lbs)
    
    
    def proposal_validator(self, str_input, proposal_type):
        str_input = str_input.lower()
        
        if proposal_type == 'tmp':
            if not str_input.startswith('tmp') and not str_input.startswith('temp') and not str_input.startswith('test'):
                raise ValueError("Proposal name does not start with test, temp or tmp")
                
        if proposal_type == 'inhouse':
            if not str_input=='' and not str_input.startswith('blc') and not str_input.startswith('ih'):
                raise ValueError("Proposal name does not start with blc or ih")
                
        if proposal_type == 'visitor':
            if str_input.startswith('tmp') or str_input.startswith('temp') or str_input.startswith('test') or \
               str_input.startswith('blc') or str_input.startswith('ih'):
                raise ValueError("Proposal name should neither start with \nblc or ih (inhouse) nor test, temp or tmp")
    
        return str_input
        
    
    def configure(self, proposal_type, proposal=None, sample=None, dataset=None, comment=None, use_lbs=False):
        """
        Configures proposal name, sample name, dataset name and the path extension when using a LBS
        """
        scan_saving_config = current_session.scan_saving.scan_saving_config
        scan_saving        = current_session.scan_saving
        
        log_info(self,"configure() entering")
        
        if proposal_type == 'visitor':
            root_path = "visitor_data_root"
        elif proposal_type == 'inhouse':
            root_path = "inhouse_data_root"
        elif proposal_type == 'tmp':
            root_path = "tmp_data_root"
            
        # configure the base path extension if an LBS is used    
        self.parameters.lbs_active = use_lbs
        if self.parameters.lbs_active and 'lbsram' not in scan_saving_config[root_path]:
            scan_saving_config[root_path] = '/lbsram'+ scan_saving_config[root_path]
        elif not self.parameters.lbs_active and 'lbsram' in scan_saving_config[root_path]:
            scan_saving_config[root_path] = '/' + '/'.join(scan_saving_config[root_path].split('/')[2:])
        
        # configure data policy
        if proposal != None:
            if scan_saving.proposal != proposal:
                self.proposal_validator (proposal, proposal_type)
                scan_saving.newproposal(proposal)
        
        if sample != None:
            if scan_saving.sample != sample:
                scan_saving.newsample(sample)
                
        if dataset != None:           
            if scan_saving.dataset != dataset:
                scan_saving.newdataset(dataset)
                
        if comment != None:
            self.parameters.comment = comment
                    
        # Apply the correct directory mapping if specified for the detector
        if len(self.tomo_ccd.detector.directories_mapping) > 0:
            for proposal_type in self.tomo_ccd.detector.directories_mapping_names:
                if proposal_type in scan_saving.base_path:
                    self.tomo_ccd.detector.select_directories_mapping(proposal_type)    
        
        log_info(self,"configure() leaving")

    
    
    def clean_scan_saving(self):
        """
        Clean-up scan saving from old tomo definitions.
        Will disapear soon!
        """
        log_info(self,"apply_scan_saving() entering")
        
        
        current_session.scan_saving.images_path_template = 'scan{scan_number}'    
        current_session.scan_saving.images_prefix = '{img_acq_device}_'
        
            
        log_info(self,"apply_scan_saving() leaving")
        
        
