from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

class TomoSequencePreset:
    
    def __init__(self, name, config):
        
         # init logging
        self.log_name = name+'.sequence_preset'
        global_map.register(self, tag=self.log_name)
        log_info(self,"__init__() entering")
        
        self.name = name
        self.config = config
        
        log_info(self,"__init__() leaving")
        
    def prepare(self, tomo):
        pass
        
    def start(self):
        pass
        
    def stop(self):
        pass
