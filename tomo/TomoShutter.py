import sys
import gevent
import numpy as np
import time
from enum import Enum

import PyTango

import bliss
from bliss import global_map
from bliss.common import session
from bliss.common.standard import *
from bliss.common.logtools import log_info,log_debug

from tomo.TomoParameters import TomoParameters

class ShutterType(Enum):
    CCD  = 1
    SOFT = 2
    NONE = 3

class TomoShutter(TomoParameters):
    def __init__(self, tomo_name, tomo_config, tomo_ccd, *args, **kwargs):
        # init logging
        self.name = tomo_name+".shutter"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        #print(tomo_config)
        config = tomo_config["shutter"]
        
        self.tomo_ccd  = tomo_ccd
        self.tomo_name = tomo_name
        
        self.beam_shutter = config.get("beam_shutter", None)
        self.fast_shutter = config.get("fast_shutter", None)
        self.ccd_shutter  = config.get("ccd_shutter", False)
        self.ccd_shutter_time = config.get("ccd_shutter_time", None)

        self.init()
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':shutter_parameters'
        shutter_defaults = {}
        shutter_defaults['dark_images_with_beam_shutter']  = False
        
        # Initialise the TomoParameters class
        super().__init__(param_name, shutter_defaults)
        
        log_info(self,"__init__() leaving")
    
    def init(self):
        """ Update shutter_used (shutter type used)
            mainly for the case shutter is ccd driven
        """
        if self.fast_shutter is None:
            self.shutter_used = ShutterType.NONE
        else:
            self.shutter_used = ShutterType.SOFT

        if self.ccd_shutter is True:
            if self.tomo_ccd.use_shutter() is True:
                self.shutter_used = ShutterType.CCD

    def __info__(self):
        beam_shname = self.beam_shutter is None and str(None) or self.beam_shutter.name
        fast_shname = self.fast_shutter is None and str(None) or self.fast_shutter.name
        info_str  = f"Shutter infos:\n"
        info_str += f"  beam_shutter = {beam_shname}\n"
        info_str += f"  fast_shutter = {fast_shname}\n"
        info_str += f"  ccd_shutter  = {self.ccd_shutter} (control or not by CCD if available)\n"
        info_str += f"  dark_images_with_beam_shutter = {self.parameters.dark_images_with_beam_shutter}\n"
        if self.fast_shutter is not None or self.ccd_shutter is True:
            info_str += f"Fast Shutter state:\n"
            info_str += f"  controlled by = {self.shutter_used.name}\n"
            info_str += f"  closing time  = {self.fast_shutter_closing_time():.3f} sec\n"
        
        return info_str
        
    
    def fast_shutter_open(self):
        """
        Opens fast shutter
        """
        log_info(self,"fast_shutter_open() entering")
        
        if self.shutter_used == ShutterType.CCD:
            self.tomo_ccd.automatic_shutter_mode(True)
            self.tomo_ccd.set_shutter_time(self.fast_shutter_closing_time())
        else:
            if self.shutter_used == ShutterType.SOFT:
                self.fast_shutter.open()
        
        log_info(self,"fast_shutter_open() leaving")
        
    
    
    def fast_shutter_close(self):
        """
        Closes fast shutter
        """
        log_info(self,"fast_shutter_close() entering")
        
        if self.shutter_used == ShutterType.CCD:
            self.tomo_ccd.automatic_shutter_mode(False)
            self.tomo_ccd.set_shutter_time(0.0)
        else:
            if self.shutter_used == ShutterType.SOFT:
                self.fast_shutter.close()
        
        log_info(self,"fast_shutter_close() leaving")
        
        
    @property    
    def fast_shutter_state(self):
        """
        Returns fast shutter state
        """
        if self.shutter_used == ShutterType.CCD:
            return self.tomo_ccd.automatic_shutter_mode
        else:
            if self.shutter_used == ShutterType.SOFT:
                return self.fast_shutter.state
            else:
                return BaseShutterState.UNKNOWN
                
        
        
    
    def fast_shutter_closing_time(self):
        """
        Returns fast shutter closing time
        """
        log_info(self,"fast_shutter_closing_time() entering")

        if self.shutter_used == ShutterType.SOFT:
            return self.fast_shutter.closing_time
        elif self.shutter_used == ShutterType.CCD:
            if self.fast_shutter is not None:
                return self.fast_shutter.closing_time
            elif self.ccd_shutter_time is not None:
                return self.ccd_shutter_time
        return 0.0        

    def fast_shutter_used(self):
        """
        Returns fast shutter name
        """
        return self.shutter_used.name
        
    
    
    def beam_shutter_open(self):
        """
        Opens beam shutter 
        """
        log_info(self,"beam_shutter_open() entering")
        
        self.beam_shutter.open()
        
        log_info(self,"beam_shutter_open() leaving")
        
        
    def beam_shutter_close(self):
        """
        Closes beam shutter
        """
        log_info(self,"beam_shutter_close() entering")
        
        self.beam_shutter.close()
        
        log_info(self,"beam_shutter_close() leaving")
      
      
    @property    
    def beam_shutter_state(self):
        """
        Returns beam shutter state
        """
        return self.beam_shutter.state
        
        
        
    def dark_shutter_open(self):
        """
        Opens dark shutter 
        """
        
        log_info(self,"dark_shutter_open() entering")
        
        if self.parameters.dark_images_with_beam_shutter == True:
            self.beam_shutter_open()
        else:
            self.fast_shutter_open()
            
        log_info(self,"dark_shutter_open() leaving")
        
        
        
    def dark_shutter_close(self):
        """
        Closes beam shutter 
        """
        log_info(self,"dark_shutter_close() entering")
        
        if self.parameters.dark_images_with_beam_shutter == True:
            self.beam_shutter_close()
        else:
            self.fast_shutter_close()
            
        log_info(self,"dark_shutter_close() leaving")

    
    @property    
    def dark_shutter_state(self):
        """
        Returns dark shutter state
        """
        if self.parameters.dark_images_with_beam_shutter == True:
            return self.beam_shutter_state
        else:
            return self.fast_shutter_state        
        
