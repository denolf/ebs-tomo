__all__=["CScanMusstChanCalc", "_step_per_unit"]

import numpy
import gevent
import sys
import string
import datetime
from bliss.scanning.chain import AcquisitionChannel
from bliss.controllers.motor import CalcController
from bliss import setup_globals


from tomo.Tomo import ScanType

_step_per_unit = lambda mot: mot.encoder.steps_per_unit if mot.encoder else mot.steps_per_unit 

class MusstConvDataCalc(object):
    """ 
    Class to convert musst data channels into:
        - user position for encoder
        - seconds from timer
    """
    def __init__(self, name, musst_card, source_name, data_per_point, data_per_line, factor, dest_name):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        
        source_name : str
            name of the incoming data channel
        factor : int
            used to convert data to the desired unit
        dest_name : str
            name of the outcoming data channel
        """
        self.name = name
        self._musst_card = musst_card
        self._source_name = source_name
        self._data_per_point = data_per_point
        self._data_per_line = data_per_line
        self._factor = factor
        self._dest_name = dest_name
        self._raw_data = numpy.array([],dtype=numpy.int32)
        self._data = numpy.array([],dtype=numpy.int32)
        self._max_idx = self._data_per_line
        self._index = 0
    
    def __call__(self, sender, data_dict):
        """
        Function called at each new data coming from source channel
        """
        data = data_dict.get(self._source_name, None)
        
        if data is None:
            return {}

        self._raw_data = numpy.append(self._raw_data,data)
        
        if self._data_per_line is not None:
            if len(self._raw_data) > self._max_idx:
                idx_to_rm = [idx for idx in range(self._max_idx, len(self._raw_data), self._data_per_line+1)]
                self._raw_data = numpy.delete(self._raw_data, idx_to_rm)
                self._max_idx += len(idx_to_rm)*self._data_per_line
        
        self._data =  self._raw_data[self._index::self._data_per_point]
        
        calc_data = self._data / self._factor

        self._index += len(calc_data)*self._data_per_point
        
        return {f'{self._musst_card.name}:{self.name}_{self._dest_name}' : calc_data}

    @property
    def acquisition_channels(self):
        """
        Returns AcquisitionChannel associated to calculation device
        """
        return [AcquisitionChannel(f'{self._musst_card.name}:{self.name}_{self._dest_name}',numpy.float,())]     
    
class TomoTools():
    """
    Class to verify scan parameters consistancy and estimate scan duration
    """
    
    def __init__(self, tomo):
        """
        Parameters
        ----------
        tomo_ccd : object
            tomo detector object
        parameters : ParametersWardrobe 
            tomo parameters
        reference : object
            tomo reference object
        tomo : Tomo object (ex: HrTomo) 
            contains all info about tomo (hardware, parameters)
        """
        
        self.tomo_ccd = tomo.tomo_ccd 
        self.parameters = tomo.parameters
        self.reference = tomo.reference
        self.tomo = tomo
    
    def check_params(self,musst):
        """
        Checks if scan parameters are consistant
        """
                
        if self.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo_ccd.detector.camera.cam_name.lower():
            max_img = self.tomo_ccd.detector.camera.max_nb_images
            if max_img > 0 and self.parameters.tomo_n > max_img:
                raise Exception('Error, you cannot record more than {max_img} with this camera!')
            if self.tomo_ccd.detector.proxy.acq_expo_time > 0.04:
                raise Exception('Error, exposure time must be < 40 ms')
            if self.tomo_ccd.detector.proxy.acc_max_expo_time > 0.04:
                raise Exception('Error, maximum accumulated exposure time must be < 40 ms')
        
        # get the minimum latency time for the tomo scan
        min_latency_time = self.calculate_latency_time(musst)
        
        # if the given latency time is smaller than the minimum time, throw an exeception
        if self.parameters.latency_time < min_latency_time:
            raise ValueError(f'The latency time must be longer than the minimum latency time of {min_latency_time} seconds') 
            
        # Print the camera acquisition mode: SINGLE or ACCUMULATION
        print(f"The actual camera acquisition mode is set to: {self.tomo_ccd.detector.proxy.acq_mode}")


        print("<<<Parameters are consistant !>>>")

    def calculate_latency_time(self,musst):
        """
        Calculates and returns the minimum latency time for the tomo scan configuration
        """
        latency_time = 0
        
        if musst.enc_channel is not None and musst.mot_chan is not None:
            latency_time = 0.002
            if 'elmo' in musst.motor.controller.get_class_name().lower():
                readout_time = self.parameters.tomo_n*self.parameters.exposure_time/60
                if readout_time >= 5.0:
                    latency_time = 0.005
        
        # for soft scan, latency time needs to be >= 0.05 otherwise motor master is not able 
        # to send soft position triggers at the right time
        if self.parameters.scan_type == ScanType.CONTINUOUS_SOFT:
            latency_time = 0.05
        
        return latency_time
        

    def estimate_ref_scan_duration(self):
            
        # ref motor
        ref_mot = self.reference.ref_motors[0]
        if isinstance(ref_mot.controller, CalcController):
            unit_time = 0.0
        else:
            init_pos = self.reference.parameters.in_beam_position[0]
            target_pos = init_pos+self.reference.parameters.out_of_beam_displacement[0]
            # back and forth
            unit_time = 2 * self.mot_disp_time(ref_mot,target_pos-init_pos,ref_mot.velocity)
            unit_time += 2 * self.reference.settle_time

        motor_time = unit_time * (self.tomo.in_pars['nb_groups'] - 1)
        ref_img = self.parameters.ref_n * (self.tomo.in_pars['nb_groups'] - 1)
            
        if self.parameters.ref_images_at_start:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
            
        if self.parameters.ref_images_at_end:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
                
        image_time = ref_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return motor_time + image_time


    def estimate_dark_scan_duration(self):
        
        dark_img = 0
        
        if self.parameters.dark_images_at_start:
            dark_img += self.parameters.dark_n
            
        if self.parameters.dark_images_at_end:
            dark_img += self.parameters.dark_n
        
        image_time = dark_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return image_time
    
    def estimate_return_scan_duration(self,motor):
        
        scan_range = self.parameters.end_pos - self.parameters.start_pos
        
        if not self.parameters.no_return_images:
            if not self.parameters.return_images_aligned_to_refs:
                return_img = int(scan_range/90)+1
                nr_moves = int(scan_range/90)
                one_move_disp = 90
            else:
                return_img = self.tomo.in_pars['nb_groups']+1
                nr_moves = self.tomo.in_pars['nb_groups']
                step_size = scan_range/self.parameters.tomo_n 
                one_move_disp = step_size*self.parameters.ref_on
        else:
            acc_margin = self.tomo.tomo_scan.acc_margin    
            if self.tomo.tomo_scan.use_step_size:
                acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
            undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
            return_img = 0
            nr_moves = 1 
            one_move_disp = scan_range + undershoot + acc_margin

        
        one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        motor_time = (nr_moves-1)*one_move_time
        
        # if last reference group needs less projections, motor displacement will be reduced 
        if not self.parameters.no_return_images and self.parameters.return_images_aligned_to_refs:
            if not self.parameters.no_reference_groups and self.parameters.tomo_n % self.parameters.ref_on != 0:
                one_move_disp = abs(self.parameters.end_pos-self.tomo.in_pars['start_group_pos'][-1])
                one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        
        motor_time += one_move_time
        
        image_time = return_img * self.tomo.tomo_scan.in_pars['scan_point_time']
        
        return motor_time + image_time

    def step_scan_time(self,motor,start_pos,nb_points):
        """
        Estimates step motor scan time 
        """
        # one_move_time represents one step
        one_move_disp = self.tomo.tomo_scan.in_pars['scan_step_size']
        one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        scan_time = one_move_time*nb_points

        image_time = (nb_points+1) * self.tomo.tomo_scan.in_pars['scan_point_time']
        
        return scan_time + image_time
        
    def sweep_scan_time(self,motor,start_pos,end_pos,nb_points):
        """
        Estimates sweep motor scan time 
        """
        tomo_end = end_pos
        tomo_n = nb_points
        if self.tomo.in_pars['nb_groups'] > 1:
            end_pos = self.tomo.in_pars['start_group_pos'][1]
            nb_points = nb_points / self.tomo.in_pars['nb_groups'] 
        
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
            
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = (end_pos - start_pos) / nb_points
        start_disp = cst_disp + acc_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * tomo_n
        first_prepare_disp = undershoot + acc_margin
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        else:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,motor.velocity)
        prepare_disp = acc_disp
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time += self.mot_disp_time(motor,prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * (tomo_n-1)
        else:
            prepare_time += self.mot_disp_time(motor,prepare_disp,motor.velocity) * (tomo_n-1)   
        scan_time = start_time + prepare_time 
        
        # time to return to scan end position before return images acquisition 
        if not self.parameters.no_return_images:
            scan_time += self.mot_disp_time(motor,undershoot+acc_margin,motor.velocity)
            
        return scan_time
        
    def continuous_scan_time(self,motor,start_pos,end_pos):
        """
        Estimates continuous motor scan time 
        """
        end_tomo = end_pos
        if self.tomo.in_pars['nb_groups'] > 1:
            end_pos = self.tomo.in_pars['start_group_pos'][1]
        
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = end_pos - start_pos
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        prepare_time = self.mot_disp_time(motor,acc_disp/2,motor.velocity)
        
        # one_move_time represents one group
        one_move_time = start_time + prepare_time
        scan_time=one_move_time
        
        if self.tomo.in_pars['nb_groups'] > 1:
            
            prepare_disp = acc_disp
            prepare_time = self.mot_disp_time(motor,prepare_disp,motor.velocity)
            move_time = (start_time+prepare_time) * (self.tomo.in_pars['nb_groups']-1) 

            # if last reference group needs less projections, scan duration will be decreased 
            if self.parameters.tomo_n % self.parameters.ref_on != 0:
                start_pos = self.tomo.in_pars['start_group_pos'][-1]
                end_pos = end_tomo
                cst_disp = end_pos - start_pos
                start_disp = acc_disp + cst_disp
                start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
                prepare_time = self.mot_disp_time(motor,prepare_disp,motor.velocity)
                move_time = start_time + prepare_time
                
            scan_time += move_time
        
        # time to return to scan end position before return images acquisition 
        if not self.parameters.no_return_images:
            scan_time += self.mot_disp_time(motor,undershoot+acc_margin,motor.velocity)
            
        return scan_time

    def mot_disp_time(self,mot,disp,speed):
        """
        Estimates motor displacement time
        """
        half_disp = abs(disp/2)
        acc_disp = speed**2  / (2 * mot.acceleration)
        if acc_disp <= half_disp:
            return 2 * (speed / mot.acceleration + (half_disp - acc_disp) / speed)
        else:
            max_speed = numpy.sqrt(2 * mot.acceleration * half_disp)
            acc_time = max_speed / mot.acceleration
            return 2 * acc_time
            
class TopoTools(TomoTools):
    
    def sweep_scan_time(self,motor,start_pos,end_pos,nb_points):
        """
        Estimates sweep motor scan time 
        """
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = (end_pos - start_pos) / nb_points
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * nb_points
        
        first_prepare_disp = undershoot + acc_margin
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        else:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,motor.velocity)
        prepare_disp = acc_disp
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time += self.mot_disp_time(motor,prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * (nb_points-1)
        else:
            prepare_time += self.mot_disp_time(motor,prepare_disp,motor.velocity) * (nb_points-1)
            
        scan_time = start_time + prepare_time 
        
        return scan_time
        
    def continuous_scan_time(self,motor,start_pos,end_pos):
        """
        Estimates continuous motor scan time 
        """
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = end_pos - start_pos
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        prepare_time = self.mot_disp_time(motor,acc_disp,motor.velocity)
        
        one_move_time = start_time + prepare_time
        scan_time=one_move_time
        
        return scan_time
       

        
        
