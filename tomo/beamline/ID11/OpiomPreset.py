
from tomo.Tomo import ScanType
from tomo.TomoSequencePreset import TomoSequencePreset


class OpiomPreset(TomoSequencePreset):
    """
    Class to handle opiom setting
    """
    
    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        mux : controller
            multiplexer Bliss object
        mode : str
            configuration mode of multiplexer
        musst_gate : boolean
            returns True if multiplexer is configured to receive gates from musst 
        prev_val : list
            allows to store multiplexer configuration before tomo and reapply it after 
        """
        
        self.mux = config["multiplexer"]
        
        self.use_gate = False
        self.cam_name = None

        super().__init__(name, config)

    def prepare(self, tomo):
        frelon2mux = {"frelon1": "CAM1", "frelon2": "CAM2", "frelon3": "CAM3"}
        det_name = tomo.tomo_ccd.detector.name.lower()
        self.cam_name = frelon2mux.get(det_name, None)
        if self.cam_name is None:
            raise ValueError(f"Unkown camera name [{det_name}] in tomo.OpiomPreset")
        if tomo.parameters.scan_type == ScanType.SWEEP:
            self.use_gate = True
        else:
            self.use_gate = False
        
    def start(self):
        """
        Switches multiplexer outputs according to desired mode
        """
        self.mux.switch("SHUTTER", self.cam_name)
        self.mux.switch(self.cam_name, "ON")
        if self.use_gate:
            self.mux.switch("MUSST", "BTRIG")
        else:
            self.mux.switch("MUSST", "ATRIG")
        self.mux.switch("TRIGGER_MODE", "MUSST")

    def stop(self):
        """
        Switches back multiplexer ouputs to previous configuration 
        """
        pass
