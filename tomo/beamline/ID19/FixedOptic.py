# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import Timeout, sleep


from bliss.shell.cli.user_dialog import UserMsg, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.config.settings import ParametersWardrobe
from bliss.common.logtools import log_info,log_debug

from tomo.TomoOptic import TomoOptic

class FixedOptic(TomoOptic):
    """
    Class to handle standard optics with one objective.
    The class has two parts: 
    The standart methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    names : list of string
        Ordered list of optic names.
    magnification : float
        Optic magnification 
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    rotc_motor : Bliss Axis object of camera rotation motor
        The camera rotation motor for the optics
    focus_motor : Bliss Axis object of focus motor
        The focus motor for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The range of the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    
    **Parameters**:
    
    **Example yml file**::
    
        name:  10xoptic
        plugin: bliss
        class: FixedOptics
        package: id19.tomo.beamline.ID19.FixedOptics
              
        magnification: 10
        
        image_flipping_hor:  False
        image_flipping_vert: False
        
        rotc_motor:  $hrrotc
        focus_motor: $hrfocus
        focus_type: "rotation"     # translation or rotation
        focus_scan_range: 20
        focus_scan_steps: 10
        focus_lim_pos:  1000
        focus_lim_neg: -1000
    """


    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        config : dictionary
            The Bliss configuration dictionary
        """
        
        self.__name = name
        self.__config = config
        
        self._magnification = config["magnification"]
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.__name+':parameters'
        param_defaults = {}
        
        super().__init__(name, config, param_name, param_defaults)
        
        
    @property
    def description(self):
        """
        The name string the current optics 
        """
        name = "FixedOptic_"+str(self._magnification)
        return name
        
        
    #
    # fixed otics methods every otics has to implement
    #
    
    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__
    
    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        return self._magnification
            

    #
    # Specific objective handling
    #
    
    def setup(self):
        """
        Set-up the optic by chosing from the list of available fixed optics
        """
        msg = f"Fixed optic with a magnification of X{self._magnification}"
        dlg1 = UserMsg(label=msg)
        
        ct1 = Container( [dlg1], title="Optic" )
        ret = BlissDialog( [ [ ct1] ] , title='Fixed Optic Setup').show()
    
    
    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        print ("Fixed objective : magnification = X%.4f" % (self._magnification))
        
