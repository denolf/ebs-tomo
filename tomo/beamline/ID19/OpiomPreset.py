from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

from tomo.Tomo import ScanType
from tomo.TomoSequencePreset import TomoSequencePreset

class OpiomPreset(TomoSequencePreset):
    """
    Class to handle opiom setting
    """
    
    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        mux : controller
            multiplexer Bliss object
        mode : str
            configuration mode of multiplexer
        musst_gate : boolean
            returns True if multiplexer is configured to receive gates from musst 
        prev_val : list
            allows to store multiplexer configuration before tomo and reapply it after 
        """
        
        self.mux  = config['multiplexer']
        
        self.mode       = None
        self.musst_gate = None
        self.prev_val   = None
        
        super().__init__(name, config)
        
        
    def prepare(self, tomo):
        
        mode,musst_gate = self.select_opiom_mode(tomo)
        
        self.mode = mode
        self.musst_gate = musst_gate
        self.prev_val = self.mux.getGlobalStat()
        
        
    def start(self):
        """
        Switches multiplexer outputs according to desired mode
        """
        
        if self.mode == "HR_FTOMO_FRELON":
            self.mux.switch("CAMERA","CAM_HR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
        elif self.mode == "HR_FTOMO_PCO":
            self.mux.switch("CAMERA","CAM_HR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            if self.musst_gate:
                self.mux.switch("TRIGGER","MUSST_GATE")
        elif self.mode == "HR_FTOMO_BASLER":
            self.mux.switch("CAMERA","CAM_HR")
            self.mux.switch("TRIGGER","MUSST_GATE")
        elif self.mode == "MR_FTOMO_BASLER":
            self.mux.switch("CAMERA","CAM_MR")
            self.mux.switch("TRIGGER","MUSST_GATE")
        elif self.mode == "MR_FTOMO_FRELON":
            self.mux.switch("CAMERA","CAM_MR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
        elif self.mode == "MR_FTOMO_PCO":
            self.mux.switch("CAMERA","CAM_MR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            
            
    def stop(self):
        """
        Switches back multiplexer ouputs to previous configuration 
        """
        
        self.mux.switch("CAMERA",self.prev_val['CAMERA'])
        self.mux.switch("TRIGGER",self.prev_val['TRIGGER'])
        
        
        
    def select_opiom_mode(self, tomo):
        """
        Selects multiplexer mode according to detector and scan type
        """

        musst_gate = False
        opiom_mode = ''
            
        if 'pco' in tomo.tomo_ccd.detector.camera_type.lower():
            if 'hr' in tomo.config['name'].lower():
                opiom_mode = 'HR_FTOMO_PCO'
            if 'mr' in tomo.config['name'].lower():
                opiom_mode = 'MR_FTOMO_PCO'
                
        if 'basler' in tomo.tomo_ccd.detector.camera_type.lower():
            musst_gate = True
            if 'hr' in tomo.config['name'].lower():
                opiom_mode = 'HR_FTOMO_BASLER'
            if 'mr' in tomo.config['name'].lower():
                opiom_mode = 'MR_FTOMO_BASLER'

        if 'frelon' in tomo.tomo_ccd.detector.camera_type.lower():
            if 'hr' in tomo.config['name'].lower():
                opiom_mode = 'HR_FTOMO_FRELON'
            if 'mr' in tomo.config['name'].lower():
                opiom_mode = 'MR_FTOMO_FRELON'

        if tomo.parameters.scan_type == ScanType.SWEEP or tomo.parameters.scan_type == ScanType.STEP:
            if any('GATE' in  trigger for trigger in tomo.tomo_ccd.detector.available_triggers):
                musst_gate = True
            else:
                if tomo.parameters.scan_type == ScanType.SWEEP:
                    raise Exception(f"The {tomo.tomo_ccd.detector.name} camera has no gate trigger capability" + 
                                    "This mode is needed to be precise in case of sweep scan" + 
                                    "Please change camera or scan type")

        return (opiom_mode,musst_gate)
