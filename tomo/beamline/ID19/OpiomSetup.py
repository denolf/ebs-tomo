from bliss import setup_globals
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog

class OpiomSetup:
    
    def __init__(self, name, config):
        
        self.mux = config['multiplexer']
        
    def __call__(self):
        
        values = [(0,"CAM_HR"),(1,"CAM_MR")]
        defval = self.mux.getPossibleValues("CAMERA").index(self.mux.getOutputStat("CAMERA"))
        dlg_camera = UserChoice(label="CAMERA", values=values, defval=defval)
        values = [(0,"MUSST_TRIG"),(1,"MUSST_GATE")]
        defval = self.mux.getPossibleValues("TRIGGER").index(self.mux.getOutputStat("TRIGGER"))
        dlg_trigger = UserChoice(label="TRIGGER", values=values, defval=defval)
        values = [(0,"CCD"),(1,"MUSST"),(2,"SOFT")]
        defval = self.mux.getPossibleValues("SHMODE").index(self.mux.getOutputStat("SHMODE"))
        dlg_shmode = UserChoice(label="SHMODE", values=values, defval=defval)

        ret = BlissDialog( [[dlg_camera],[dlg_trigger],[dlg_shmode]], title='Opiom Setup').show()
            
        # returns False on cancel
        if ret != False:
            self.mux.switch("CAMERA",self.mux.getPossibleValues("CAMERA")[ret[dlg_camera]])
            self.mux.switch("TRIGGER",self.mux.getPossibleValues("TRIGGER")[ret[dlg_trigger]])
            self.mux.switch("SHMODE",self.mux.getPossibleValues("SHMODE")[ret[dlg_shmode]])
    
    
