from bliss.scanning.scan import ScanPreset
from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug
from bliss.config.static import get_config

from tomo.Tomo import ScanType
from tomo.TomoSequencePreset import TomoSequencePreset

class PcoTomoOpiomPreset(TomoSequencePreset):
    """
    Class to handle opiom setting
    """
    
    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        mux : controller
            multiplexer Bliss object
        mode : str
            configuration mode of multiplexer
        musst_gate : boolean
            returns True if multiplexer is configured to receive gates from musst 
        prev_val : list
            allows to store multiplexer configuration before tomo and reapply it after 
        """
        
        self.tomo_name = config['tomo_name']
        self.mux  = config['multiplexer']
        
        self.mode       = None
        self.musst_gate = None
        self.prev_val   = None
        
        super().__init__(name, config)
        
        log_info(self,"__init__() leaving")
        
    def prepare(self):
        
        tomo = get_config().get(self.tomo_name)
        mode = self.select_opiom_mode(tomo)
        
        self.mode = mode
        self.prev_val = self.mux.getGlobalStat()
        
    def start(self):
        """
        Switches multiplexer outputs according to desired mode
        """
        
        if self.mode == "HR_PCOTOMO":
            self.mux.switch("CAMERA","CAM_HR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            self.mux.switch("SHMODE","SOFT")
            self.mux.switch("SOFTSHUT","CLOSE")
        elif self.mode == "MR_PCOTOMO":
            self.mux.switch("CAMERA","CAM_MR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            self.mux.switch("SHMODE","SOFT")
            self.mux.switch("SOFTSHUT","CLOSE")
            
    def stop(self):
        """
        Switches back multiplexer ouputs to previous configuration 
        """
        
        self.mux.switch("CAMERA",self.prev_val['CAMERA'])
        self.mux.switch("TRIGGER",self.prev_val['TRIGGER'])
        self.mux.switch("SHMODE",self.prev_val['SHMODE'])
        self.mux.switch("SOFTSHUT",self.prev_val['SOFTSHUT'])
        
        
    def select_opiom_mode(self, tomo):
        """
        Selects multiplexer mode according to shutter synchronisation
        """

        opiom_mode = ''
            
        if 'hr' in tomo.config['detector_x_axis'].name.lower():
            opiom_mode = 'HR_PCOTOMO'
                
        if 'mr' in tomo.config['detector_x_axis'].name.lower():
            opiom_mode = 'MR_PCOTOMO'

        return opiom_mode
