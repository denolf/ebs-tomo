# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

#from gevent import Timeout, sleep


from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.config.settings import ParametersWardrobe
from bliss.common.logtools import log_info,log_debug

from tomo.TomoOptic import TomoOptic

class TwinOptic(TomoOptic):
    """
    Class to handle double optics of type TwinMic.
    The class has three parts: 
    The standart methods implemented for every optic,
    methods to handle the two objectives and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    magnifications : list of float
        A list with the possible magnification lenses for this objective type
    selection_motor : Bliss Axis object
        The motor used to switch the objectives.
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    
    **Parameters**:
    
    objective1_magnification : float
        The magnification for objective1. To be configured in the setup.
    objective2_magnification : float
        The magnification for objective2. To be configured in the setup.
        
    **Example yml file**::
    
        name:  twinmic
        plugin: bliss
        class: TwinOptics
        package: tomo.beamline.ID19.TwinOptics
      
        magnifications: [2, 5, 7.5, 10]
        selection_motor: $objsel
        image_flipping_hor:  True
        image_flipping_vert: False
      
        focus_motor: $hrfocus
        focus_type: "translation"     # translation or rotation
        focus_scan_steps: 20
        focus_lim_pos:  0.5
        focus_lim_neg: -0.5
    """


    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        config : dictionary
            The Bliss configuration dictionary
        """
        
        self.__name = name
        self.__config = config
        
        self.magnifications      = config["magnifications"]
        self.selection_motor     = config["selection_motor"]
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.__name+':parameters'
        param_defaults = {}
        param_defaults ['objective1_magnification']  = self.magnifications[0]
        param_defaults ['objective2_magnification']  = self.magnifications[0]
        
        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)


    @property
    def description(self):
        """
        The name string the current optics 
        """
        name = "TwinMic_"+str(self.magnification)
        return name

        
    #
    # standart otics methods every otics has to implement
    #
    
    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__
    
    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        objective = self.objective()
        if objective == 1:
            return self.parameters.objective1_magnification
        else:
            return self.parameters.objective2_magnification
            

    #
    # Specific objective handling
    #
    
    def setup(self):
        """
        Set-up the magnification for the two objectives mounted.
        They can be chosen from the list of possible magnifications.
        """
        value_list = []
        for i in self.magnifications:
            value_list.append( (i, 'X'+str(i)) )
            
        # get the actual magnification values as default
        default1 = 0
        default2 = 0
        
        for i in range (0, len(value_list)):
            if self.parameters.objective1_magnification == value_list[i][0]:
                default1 = i
            if self.parameters.objective2_magnification == value_list[i][0]:
                default2 = i
        
        dlg1 = UserChoice(values=value_list, defval=default1)
        dlg2 = UserChoice(values=value_list, defval=default2)
        
        ct1 = Container( [dlg1], title="Objective1" )
        ct2 = Container( [dlg2], title="Objective2" )
        ret = BlissDialog( [ [ ct1, ct2] ] , title='TwinMic Setup').show()
        
        # returns False on cancel
        if ret != False:
            # magnification objective 1
            self.parameters.objective1_magnification = float(ret[dlg1])
            # magnification objective 2
            self.parameters.objective2_magnification = float(ret[dlg2])
    
    
    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            ojective = self.objective
            magnification = self.magnification
            print ("Objective %d selected with a magnification of X%s" % ojective, str(magnification) )
        
        except ValueError as err:
            print('Optics indicates a problem:\n', err)
        
    @property
    def objective(self):
        """
        Reads and sets the current objective (1 or 2)
        """
        return self._objective_state()
        
    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1 or 2
        """
        if value < 1 or value > 2:
            raise ValueError("Only the objectives 1 and 2 can be chosen!")
            
        if value == 1:
            self.selection_motor.hw_limit(-1) # negative limit
        else:
            if value == 2:
                self.selection_motor.hw_limit(1) # positive limit
        
    
    def _objective_state(self):
        """
        Evaluates which obective is currently used and returns its value (1or 2)
        """
        current_objective = None
        
        # Get current objective
        lim_high = self.selection_motor.state.LIMPOS
        lim_low  = self.selection_motor.state.LIMNEG
        
        if lim_high is True and lim_low is False:
            current_objective = 2
        else:
            if lim_high is False and lim_low is True:
                current_objective = 1
            else:
                if lim_high is True and lim_low is True:
                    raise ValueError("No objective selected\nBoth limits are active at the same time, very strange!")
                else:
                    if lim_high is False and lim_low is False:
                        raise ValueError("No objective selected\nNo active limit!")
        
        return current_objective
                        
    
    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        # the focus scan range is dependent on the magnigication
        if self.magnification == 2:
            self.focus_scan_range = 0.2
        else:
            self.focus_scan_range = 0.025
            
        scan_params = super().focus_scan_parameters()
        return scan_params
    
