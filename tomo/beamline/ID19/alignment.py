import sys
import gevent
import numpy as np
import time

from bliss.common import scans
from bliss.common.scans import ascan, pointscan
from bliss.common.plot import plotselect
from bliss.common.cleanup import error_cleanup, capture_exceptions, axis as cleanup_axis
from bliss.scanning.scan_tools import peak, goto_peak
from bliss.controllers.lima.roi import Roi
from bliss.shell.standard import umv, mv
from bliss.config.static import get_config

    

def tomo_alignxc(tomo, svg="ssvg", shg="sshg", xc_start=100, xc_end=900, save=False):
    
    # take dark image
    dark_n = 1
    dark_scan = tomo.dark_scan(dark_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    dark_images = get_images(dark_scan, tomo.tomo_ccd.detector)
    
    # do alignment scan
    al_scan = align_scan(tomo.tomo_ccd.detector, tomo.parameters.exposure_time,
                         tomo.tomo_ccd.calculate_image_pixel_size(tomo.optic.magnification)
                         tomo.detector_axis,
                         svg=svg, shg=shg, xc_start=xc_start, xc_end=xc_end, save=save)
    # read back the aquired images
    al_images = get_images(al_scan, tomo.tomo_ccd.detector)
    
    print(al_images)
    
    # calculate the XC correction
        # Needs calculation method from C.Nemoz!!!!!!!!!
        
    
    
def tomo_align(tomo, save=False):
    
    # move rotation axis to 0
    umv (tomo.rotation_axis, 0)
    
    # take dark image
    dark_n = 1
    dark_scan = tomo.dark_scan(dark_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    dark_images = get_images(dark_scan, tomo.tomo_ccd.detector)
    
    # take reference image
    ref_n = 1
    ref_scan = tomo.ref_scan(ref_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    ref_images = get_images(ref_scan, tomo.tomo_ccd.detector)
    
    # do alignment scan
    al_scan = align_scan(tomo.tomo_ccd.detector, tomo.parameters.exposure_time, 
                         tomo.rotation_axis, save=save)
    # read back the aquired images
    al_images = get_images(al_scan, tomo.tomo_ccd.detector)
        
    # prepare the images
    radio0   = (al_images[0] - dark_images[0]) / ref_images[0]
    radio180 = (al_images[1] - dark_images[0]) / ref_images[0]
    
    # flip the radio180 for the calculation
    radio_180 = numpy.fliplr(radio180.copy())
    
    # musst I treat the nan and inf values??????? 
    
    radio0[np.isnan(radio0)]     = 0.0001        
    radio180[np.isnan(radio180)] = 0.0001
    
    print(radio0)
    print(radio180)
    
    # calculate lateral correction
        # How to import from NABU?????????
    #pixel_cor = find2DShift_fft(radio0, radio180)
    
    #distance from source to sample   = L1(mm)
    #distance from source to detector = L2(mm)
    #size of the image pixel          = s(mm/pixel)
    #dmot(mm) = L1/L2*s*pixel_cor
    
    cor_factor = tomo.parameters.source_sample_distance / 
                 (tomo.parameters.source_sample_distance + tomo.parameters.sample_detector_distance) *
                 tomo.tomo_ccd.calculate_image_pixel_size(tomo.optic.magnification)
    pos_cor = cor_factor * pixel_cor
    print (f"Lateral alignment position correction: {pos_cor}")    
    
    # apply correction
    
        # corretion to be applied to which motor???
    


def tomo_dofocus(tomo, save=False):
    dofocus(tomo.tomo_ccd.detector, tomo.optic, tomo.parameters.exposure_time, save=save)
    


def dofocus(detector, optic, expo_time, save=False):
    print(detector.name)
    print(optic.__info__())
    
    # get motor to move paper
    scr = get_config().get("scr")
    
    # prepare roi counters for statistics
    image_sizes = detector.image.sizes
    focus_roi = Roi(0,0, image_sizes[2], image_sizes[3])
    
    det_rois = detector.roi_counters
    det_rois["focus_roi"] = focus_roi
    
    std = detector.roi_counters.get_single_roi_counters("focus_roi").std
    avg = detector.roi_counters.get_single_roi_counters("focus_roi").avg
    counters = (std, avg, detector.image)
    
    # prepare the focus scan parameters
    scan_pars = optic.focus_scan_parameters()
    
    scan_range = scan_pars["focus_scan_range"]
    scan_steps = scan_pars["focus_scan_steps"]
    focus_type = scan_pars["focus_type"]
    focus_motor = optic.focus_motor()
    
    if focus_type == "Rotation" and optic.magnification < 0.7:
        scan_range /= 2
        scan_steps *= 2
    
    start = focus_motor.position - scan_range
    stop  = focus_motor.position + scan_range
    
    # clean-up: move back focus motor, delete roi counters, move out paper
    restore_list = (cleanup_axis.POS,)
    with error_cleanup(*[focus_motor, scr], restore_list=restore_list):
        with capture_exceptions(raise_index=0) as capture:
            with capture():
                
                # move in the paper
                print ("Move in paper")
                #umv(scr, -68.0)
                
                # execute focus scan
                scan = ascan(focus_motor, start, stop, scan_steps, expo_time, counters, title="focus scan", save=save)
                    
                # get the position on the maximum of the standard deviation
                plotselect(std)
                pos_std_max = peak()
                
                # Verify that the position values id not the first or the last position value
                pos_list = scan.get_data()[focus_motor.name]
                if pos_std_max == pos_list[0] or pos_std_max == pos_list[-1]:
                    raise ValueError ("Focus value is not in the scan range! Move towards the best focus by hand!") 
                
                # Always move to the best focus from the bottom
                mv(focus_motor, pos_list[0])
                # move focus motor to maximum
                print ("Moving to best focus value:")
                goto_peak()
                
                # move out paper
                print ("Move out paper")
                #umv(scr, -130.0)
                
                # delete roi counters
                del det_rois["focus_roi"]
                
                # put back old plotselect
                # but how????????????
            
            # test if an error has occured
            if len(capture.failed) > 0:
                # delete roi counters
                del det_rois["focus_roi"]
    
    
    
def alignxc_scan(detector, expo_time, image_pixel_size, xc="xc", svg="ssvg", shg="sshg", xc_start=100, xc_end=900, save=False):
    if isinstance(xc, str):
        xc = get_config().get(xc)
    if isinstance(svg, str):
        svg = get_config().get(svg)
    if isinstance(shg, str):
        shg = get_config().get(shg)
    
    # save current slit positions
    vg_pos = svg.position
    hg_pos = shg.position
    print (f"Current slit gap positions: vertical={vg_pos}, horizontal={hg_pos}")
    print (f"Current xc positions:       xc={xc.position}")
    
    # clean-up: move back xc motor, open_slits
    restore_list = (cleanup_axis.POS,)
    with error_cleanup(*[xc, svg, shg], restore_list=restore_list):
        # close the slits
        if image_pixel_size < 3:
            umv(svg, 0.2,
                shg, 0.2)
        else:
            umv(svg, 1.0,
                shg, 1.0)
    
        # scan xc
        npoints = 5
        scan = ascan(xc, xc_start, xc_end, npoints, expo_time, detector.image, title="align xc scan", save=save)
        
        ## read back the aquired images
        #scan_images=[]
        #lima_data = scan.get_data()[detector.image]
        
        #if npoints < lima_data.buffer_max_number:
            #for i in range (npoints + 1):
                #scan_images.append(lima_data.get_image(i))
    
        # open the slits
        umv(svg, vg_pos,
            shg, hg_pos)

        # return images
        return scan



def align_scan(detector, expo_time, rot, save=False):

    # clean-up: move back rot
    restore_list = (cleanup_axis.POS,)
    with error_cleanup(rot, restore_list=restore_list):
        # scan rot
        rot_pos_list=[0.0, 180.0]
        scan = pointscan(rot, rot_pos_list, expo_time, detector.image, title="align scan", save=save)
        
        return scan



def get_images(scan, detector):
    # read back the scan images aquired
    scan_images=[]
    lima_data = scan.get_data()[detector.image]
    npoints = lima_data.last_index
        
    if npoints < lima_data.buffer_max_number:
        for i in range(npoints):
            scan_images.append(lima_data.get_image(i))
        
        return scan_images
    
    else:
        ValueError("Cannot read images! Not enough images in the Lima image buffer") 
