from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.chain import ChainNode
from bliss import setup_globals

#class PcoTomoLimaChainNode(ChainNode):
    
    #def get_acquisition_object(self, acq_params, ctrl_params=None):

        ## --- Warn user if an unexpected is found in acq_params
        #expected_keys = [
            #"acq_mode",
            #"acq_nb_frames",
            #"acq_expo_time",
            #"acq_trigger_mode",
            #"acc_max_expo_time",
            #"prepare_once",
            #"start_once",
            #"wait_frame_id",
            #"acc_time_mode",
            #"latency_time",
            #"save",
            #"stat_history",
            #"saving_format",  # ---Temporary fix should be moved to controller parameters
            #"saving_frame_per_file",  # ---Temporary fix should be moved to controller parameters
            #"saving_suffix",  # ---Temporary fix should be moved to controller parameters
        #]
        #for key in acq_params.keys():
            #if key not in expected_keys:
                #print(
                    #f"=== Warning: unexpected key '{key}' found in acquisition parameters for PcoTomoLimaMaster({self.controller}) ==="
                #)

        ## --- MANDATORY PARAMETERS -------------------------------------
        #acq_mode = acq_params["acq_mode"]
        #acq_nb_frames = acq_params["acq_nb_frames"]
        #acq_expo_time = acq_params["acq_expo_time"]
        #acq_trigger_mode = acq_params["acq_trigger_mode"]
        #prepare_once = acq_params["prepare_once"]
        #start_once = acq_params["start_once"]

        ## --- PARAMETERS WITH DEFAULT VALUE -----------------------------
        #acc_max_expo_time = acq_params.get("acc_max_expo_time",1)
        #stat_history = acq_params.get("stat_history", acq_nb_frames)
        #wait_frame_id = acq_params.get("wait_frame_id", range(acq_nb_frames))
        #acc_time_mode = acq_params.get("acc_time_mode", "LIVE")
        #latency_time = acq_params.get("latency_time", 0)
        #save_flag = acq_params.get(
            #"save", True
        #)  # => key != AcqObj keyword  and location not well defined  !

        ## ---Temporary fix should be moved to controller parameters --------
        #saving_format = acq_params.get("saving_format", "EDF")
        #saving_frame_per_file = acq_params.get("saving_frame_per_file", 1)
        #saving_suffix = acq_params.get("saving_suffix", ".edf")


        #return PcoTomoLimaAcquisitionMaster(
            #self.controller,
            #acq_mode=acq_mode,
            #acq_nb_frames=acq_nb_frames,
            #acq_expo_time=acq_expo_time,
            #acq_trigger_mode=acq_trigger_mode,
            #acc_time_mode=acc_time_mode,
            #acc_max_expo_time=acc_max_expo_time,
            #latency_time=latency_time,
            #save_flag=save_flag,
            #prepare_once=prepare_once,
            #start_once=start_once,
            #wait_frame_id=wait_frame_id,
            #saving_statistics_history=stat_history,
            #saving_format=saving_format,  # => temp fix should be moved to controller parameters
            #saving_frame_per_file=saving_frame_per_file,  # => temp fix should be moved to controller parameters
            #saving_suffix=saving_suffix,  # => temp fix should be moved to controller parameters
            #ctrl_params=ctrl_params,
            #)



class PcoTomoLimaAcquisitionMaster(LimaAcquisitionMaster):
    
    def __init__(self, device, ctrl_params=None, **acq_params):
        LimaAcquisitionMaster.__init__(self, device, ctrl_params=None, **acq_params)
    
    def start(self):
        LimaAcquisitionMaster.start(self) 
        if self._LimaAcquisitionMaster__sequence_index > 0:
            setup_globals.config.get('multiplexer_tomo').switch('IOTRIG','ON')
    
    def wait_ready(self):
        setup_globals.config.get('multiplexer_tomo').switch('IOTRIG','OFF')
        LimaAcquisitionMaster.wait_ready(self) 
