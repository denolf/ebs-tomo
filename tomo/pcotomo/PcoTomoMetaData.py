import sys
import gevent
import numpy as np
import time

import bliss
from bliss import global_map, current_session
from bliss.common import session
from bliss.common.logtools import log_info,log_debug


class PcoTomoMetaData:
    """
    Prepares the meta data for tomo scans.
    The meta data will be returned as a nested dictionary.
    For every tomo scan the dictionary gets injected as scan info in the Bliss scan object.
    The meta data will be added to the Bliss scan file under "scan_meta/technique"
    """
    
    def __init__(self, tomo_name, tomo):
        # init logging
        self.__name = tomo_name+".metadata"
        global_map.register(self, tag=self.__name)
        log_info(self,"__init__() entering")
        
        self.tomo = tomo
        
    def tomo_scan_info(self):
        """
        Builds and returns the nested meta data dictionary.
        """
        
        scan_info = {}
        
        # Basic tomo scan parameters
        scan = {}
        scan['name']        = self.tomo.saving.parameters.scan_name
        scan['start_angle']        = self.tomo.parameters.start_pos
        scan['scan_range']  = self.tomo.parameters.trange
        scan['scan_time']  = self.tomo.parameters.time
        scan['tomo_n']      = self.tomo.parameters.tomo_n
        scan['nb_tomo']      = self.tomo.parameters.ntomo
        scan['nb_loop']      = self.tomo.parameters.nloop
        scan['user_output']      = self.tomo.parameters.user_output
        scan['start_turn']      = self.tomo.parameters.start_turn
        scan['waiting_turns']      = self.tomo.parameters.nwait
        scan['no_saving_between_tomo']      = self.tomo.parameters.noread
        
        
        
        scan['exposure_time'] = {'@data': self.tomo.tomo_scan.in_pars['exposure_time'] * 1000,
                                 '@unit': "ms"}
        scan['latency_time']  = {'@data': self.tomo.parameters.latency_time * 1000,
                                 '@unit': "ms"}
        scan['fast_shutter_time']  = {'@data': self.tomo.tomo_scan.in_pars['fast_shut_time'] * 1000,
                                 '@unit': "ms"}
        scan['synchronized_shutter_time']  = {'@data': self.tomo.tomo_scan.in_pars['sync_shut_time'] * 1000,
                                 '@unit': "ms"}
        scan['energy']        = self.tomo.parameters.energy
        scan['source_sample_distance']   = {'@data': self.tomo.parameters.source_sample_distance,
                                            '@unit': "mm"}
        scan['sample_detector_distance'] = {'@data': self.tomo.parameters.sample_detector_distance,
                                            '@unit': "mm"}
        scan['sequence']      = self.tomo.sequence

        # comment
        scan['comment'] = self.tomo.saving.parameters.comment
        
        scan_info['scan'] = scan
        
        # Scan configuration flags
        scan_flags = {}
        scan_flags['dark_images_at_start']   = self.tomo.parameters.dark_images_at_start
        scan_flags['dark_images_at_end']     = self.tomo.parameters.dark_images_at_end
        scan_flags['dark_images_in_accumulation'] = self.tomo.parameters.dark_images_in_accumulation
        scan_flags['ref_images_at_start']    = self.tomo.parameters.ref_images_at_start
        scan_flags['ref_images_at_end']      = self.tomo.parameters.ref_images_at_end
        scan_flags['return_to_start_pos']    = self.tomo.parameters.return_to_start_pos
        scan_info['scan_flags'] = scan_flags
        
        # Detector image parameters
        
        # read image parameters
        image_params = self.tomo.tomo_ccd.get_image_parameters()
        scan_info['detector'] = image_params
        
        # optic parameters
        optic = {}
        optic['name']          = self.tomo.optic.description
        optic['type']          = self.tomo.optic.type
        optic['magnification'] = self.tomo.optic.magnification
        optic['sample_pixel_size '] = {'@data': self.tomo.tomo_ccd.calculate_image_pixel_size(self.tomo.optic.magnification),
                                       '@unit': "mm"}
        scan_info['optic'] = optic
        
        # saving parameters
        saving={}
        saving['path']              = current_session.scan_saving.get_path()
        saving['image_file_format'] = self.tomo.saving.parameters.image_file_format
        saving['frames_per_file']   = self.tomo.saving.parameters.frames_per_file
        scan_info['saving'] = saving
        
        # dark parameters
        dark={}
        dark['dark_images_in_accumulation'] = self.tomo.parameters.dark_images_in_accumulation
        dark['dark_n']      = self.tomo.parameters.dark_n
        scan_info['dark'] = dark
        
        # reference displacement
        
        # get motor names
        names=[]
        for i in self.tomo.fasttomo.reference.ref_motors:
            names.append(i.name)
            
        ref={}
        ref['ref_n']       = self.tomo.parameters.ref_n
        ref['motors']      = names
        ref['displacement'] = {'@data': self.tomo.fasttomo.reference.parameters.out_of_beam_displacement,
                               '@unit': "mm"}
        scan_info['reference'] = ref
        
        # Create the entry in the HDF5 file
        meta_data = {'technique' : scan_info}
        
        return meta_data
        
        
