import gevent
from bliss.scanning.acquisition.motor import JogMotorMaster
from bliss.common.cleanup import error_cleanup
from bliss.common.axis import DEFAULT_POLLING_TIME

class PcoTomoMotorMaster(JogMotorMaster):

    def __init__(self, axis, start, speed, end_func=None, undershoot=None, start_margin=0):
        JogMotorMaster.__init__(self, axis, start, speed, end_func, undershoot)
        self.undershoot_start_margin = start_margin
       
    def _calculate_undershoot(self,pos):
        if self.undershoot is None:
            acctime = abs(float(self.jog_speed) / self.movable.acceleration)
            self.undershoot = abs(self.jog_speed) * acctime / 2 
        pos -= (self.undershoot + self.undershoot_start_margin)
        return pos

    def stop(self):
        JogMotorMaster.stop(self)
        pos = self.movable.position / 360
        pos = (pos-int(pos))*360
        self.movable.dial = self.movable.sign*pos
        self.movable.position = self.movable.sign*self.movable.dial

        
    def _end_jog_watch(self, polling_time):
        try:
            while self.movable.is_moving:
                stopFlag = True
                try:
                    if self.end_jog_func is not None:
                        stopFlag = not self.end_jog_func(self.movable)
                    else:
                        stopFlag = False
                    if stopFlag:
                        self.stop()
                        break
                    gevent.sleep(polling_time)
                except:
                    self.stop()
                    raise
        finally:
            self._JogMotorMaster__end_jog_task = None


class PcoTomoIterMotorMaster(JogMotorMaster):

    def __init__(self, axis, start, speed, end_func=None, undershoot=None, start_margin=0):
        JogMotorMaster.__init__(self, axis, start, speed, end_func, undershoot)
        self.undershoot_start_margin = start_margin
        
    def __iter__(self):
        self._iter_index = 0
        while self._iter_index <= self.slaves[0].vars['V_NLOOP']-1:
            yield self
            self._iter_index += 1
       
    def _calculate_undershoot(self,pos):
        if self.undershoot is None:
            acctime = abs(float(self.jog_speed) / self.movable.acceleration)
            self.undershoot = abs(self.jog_speed) * acctime / 2 
        pos -= (self.undershoot + self.undershoot_start_margin)
        return pos
        
    def prepare(self):
        if self._iter_index == 0:
            JogMotorMaster.prepare(self)
        else:
            return
    
    def start(self):
        with error_cleanup(self.stop):
            if self._iter_index == 0:
                self.movable.jog(self.jog_speed)
            if self._iter_index == self.slaves[0].vars['V_NLOOP']-1:
                self.__end_jog_task = gevent.spawn(self._end_jog_watch, polling_time=DEFAULT_POLLING_TIME)
                self.__end_jog_task.join()
            
    def stop(self):
        JogMotorMaster.stop(self)
        pos = self.movable.position / 360
        pos = (pos-int(pos))*360
        self.movable.dial = self.movable.sign*pos
        self.movable.position = self.movable.sign*self.movable.dial
        
    def _end_jog_watch(self, polling_time):
        try:
            while self.movable.is_moving:
                stopFlag = True
                try:
                    if self.end_jog_func is not None:
                        stopFlag = not self.end_jog_func(self.movable)
                    else:
                        stopFlag = False
                    if stopFlag:
                        self.stop()
                        break
                    gevent.sleep(polling_time)
                except:
                    self.stop()
                    raise
        finally:
            self._JogMotorMaster__end_jog_task = None

    def wait_ready(self):
        return True

