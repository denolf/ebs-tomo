from bliss.scanning.acquisition.musst import MusstAcquisitionSlave,MusstAcquisitionMaster,_MusstAcquisitionSlave
from bliss.scanning.chain import profile
from bliss.common.event import dispatcher
import gevent

class PcoTomoMusstAcquisitionMaster(MusstAcquisitionMaster):
    
    def __init__(self,musst_dev,program,program_start_name,program_abort_name,vars,program_template_replacement):
        MusstAcquisitionMaster.__init__(self,musst_dev,program,program_start_name=program_start_name,program_abort_name=program_abort_name,vars=vars,program_template_replacement=program_template_replacement)
        
    def __iter__(self):
        self._iter_index = 0
        self.next_vars = self.vars
        while self._iter_index <= self.vars['V_NLOOP']-1:
            yield self
            self._iter_index += 1
    
    def prepare(self):
        if self._iter_index == 0:
            MusstAcquisitionMaster.prepare(self)
        else:
            return
    
    def start(self): 
        if self._iter_index == 0:
            MusstAcquisitionMaster.start(self)
        else:
            return
        
    def wait_ready(self):
        if self._iter_index <= self.vars['V_NLOOP']-1:
            return True
        else:
            MusstAcquisitionMaster.wait_ready(self)

class _PcoTomoMusstAcquisitionSlave(_MusstAcquisitionSlave):
    
    def __init__(self,musst,store_list=None):
        _MusstAcquisitionSlave.__init__(self, musst, store_list=store_list)
    
    def _check_reading_task(self):
        if self._master is None or self._master._iter_index <= self._master.vars['V_NLOOP']-1: 
            return True
        else:
            _MusstAcquisitionSlave._check_reading_task(self)
            
    def _start(self, stats_dict):
        with profile(stats_dict, self.name, "start"):
            dispatcher.send("start", self)
            self.start()
            if self._check_reading_task() and self._reading_task is None:
                self._reading_task = gevent.spawn(self.reading)

    def wait_reading(self):
        if self._master is None or self._master._iter_index <= self.parent.vars['V_NLOOP']-1: 
            return True
        else:
           _MusstAcquisitionSlave.wait_reading(self)

        
def PcoTomoMusstAcquisitionSlave(musst_dev,program=None,program_start_name=None,program_abort_name=None,store_list=None,vars=None,program_template_replacement=None):
    
    if program is None:
        return _PcoTomoMusstAcquisitionSlave(musst_dev, store_list=store_list)
    else:
        master = PcoTomoMusstAcquisitionMaster(
            musst_dev,
            program=program,
            program_start_name=program_start_name,
            program_abort_name=program_abort_name,
            vars=vars,
            program_template_replacement=program_template_replacement,
        )
        return _PcoTomoMusstAcquisitionSlave(master,store_list=store_list)
