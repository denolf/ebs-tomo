from bliss.scanning.scan import ScanPreset
from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

from tomo.Tomo import ScanType

class PcoTomoOpiomPreset(ScanPreset):
    """
    Class to handle opiom setting
    """
    
    def __init__(self, tomo):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        mux : controller
            multiplexer Bliss object
        mode : str
            configuration mode of multiplexer
        prev_val : list
            allows to store multiplexer configuration before tomo and reapply it after 
        """
        
        # init logging
        self.name = tomo.tomo_name+".opiom"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        
        ScanPreset.__init__(self)
        self.mux = session.get_current_session().config.get('multiplexer_tomo')
        
        self.mode = self.select_opiom_mode(tomo)
        
        self.prev_val = self.mux.getGlobalStat()
        
        log_info(self,"__init__() leaving")
        
    def prepare(self,scan):
        """
        Switches multiplexer outputs according to desired mode
        """
        
        if self.mode == "HR_PCOTOMO":
            self.mux.switch("CAMERA","CAM_HR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            self.mux.switch("SHMODE","SOFT")
            self.mux.switch("SOFTSHUT","CLOSE")
        elif self.mode == "MR_PCOTOMO":
            self.mux.switch("CAMERA","CAM_MR")
            self.mux.switch("TRIGGER","MUSST_TRIG")
            self.mux.switch("SHMODE","SOFT")
            self.mux.switch("SOFTSHUT","CLOSE")
            
    def stop(self,scan):
        """
        Switches back multiplexer ouputs to previous configuration 
        """
        
        self.mux.switch("CAMERA",self.prev_val['CAMERA'])
        self.mux.switch("TRIGGER",self.prev_val['TRIGGER'])
        self.mux.switch("SHMODE",self.prev_val['SHMODE'])
        self.mux.switch("SOFTSHUT",self.prev_val['SOFTSHUT'])
        
        
    def select_opiom_mode(self, tomo):
        """
        Selects multiplexer mode according to shutter synchronisation
        """

        opiom_mode = ''
            
        if 'hr' in tomo.config['name'].lower():
            opiom_mode = 'HR_PCOTOMO'
                
        if 'mr' in tomo.config['name'].lower():
            opiom_mode = 'MR_PCOTOMO'

        return opiom_mode
