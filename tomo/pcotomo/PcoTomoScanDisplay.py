from bliss.scanning.chain import AcquisitionChain, AcquisitionChannel, AcquisitionMaster
from bliss.scanning.scan import Scan,ScanState, DataWatchCallback, WatchdogCallback
from bliss import setup_globals
from bliss.data.node import is_zerod
from bliss.common import session, scans
import sys
import gevent
import numpy as np
import bliss
import time
import datetime
from bliss.common import axis
from bliss.common import event
from bliss.config.static import get_config

def BOLD(msg):
    return "\033[1m{0}\033[0m".format(msg)

class PcoTomoScanDisplay(DataWatchCallback):
    """
    Class to display data during a tomo scan
    """

    HEADER = (
        "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}"
    )

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, trigger_name=None, motors=list(), limas=list()):
        """
        Initialisation of all scan members
        """
        self.__motors = motors
        self.__motor_names = [motor.name for motor in motors]
        self.__limas = limas
        self.__trig_name = trigger_name

    def on_state(self, state):
        """
        If True "on_scan_data" will be called at 
        each scan state: PREPARING, STARTING and STOPPING
        """
        return True

    def on_scan_new(self, scan, info):
        """
        Displays scan info: scan number, data saving path, user name, ...
        Called when the scan is about to start
        
        scan -- is the scan object
        info -- is the dict of information about this scan
        """

        print(self.HEADER.format(**info))
        self.__state = None
        self.__infos = dict()
        for name in self.__motor_names:
            self.__infos[name] = "----.---"
        if self.__trig_name is not None:
            self.__infos["trig"] = 0
        for lima in self.__limas:
            self.__infos[lima.name] = 0
        self.__start_time = datetime.datetime.fromtimestamp(time.time())
        self.__last_saved = 0
        self.__data_saving_rate = 0
        self.__new_trigger = False
        
    def on_scan_end(self, info):
        """
        Displays scan execution time.
        Called at the end of the scan.
        """

        start = datetime.datetime.fromtimestamp(info["start_timestamp"])
        end = datetime.datetime.fromtimestamp(time.time())
        msg = "\nFinished (took {0})\n".format(end - start)
        print(msg)

    def on_scan_data(self, data_events, data_nodes, info):
        """
        This callback is called when new data is emitted
        Displays:
            - motor position
            - trigger number
            - last image recorded
            - last image saved
            - difference between trigger number and last image recorded
            - data saving rate in MB/s
        Checks if difference between trigger number and last image 
        recorded is lower than limit value otherwise stops the scan
        """
        # look for scan state
        state = info.get("state", None)
        if state != self.__state:
            self.__state = state
            state_msg = self.STATE_MSG.get(state, None)
            if state_msg is not None:
                print("\n{0} ...".format(state_msg))
            
        if self.__state == ScanState.PREPARING:
            # print current motor positions
            msg = ""
            for motor in self.__motors:
                name = motor.name
                value = "{0:8.3f}".format(motor.position)
                msg += "{0} {1}  ".format(BOLD(name), value)
                self.__infos[name] = value
            if len(msg):
                print(msg + "\r", end="")
            return

        if self.__state == ScanState.STARTING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    data_node = data_nodes.get(acqdev)
                    if is_zerod(data_node):
                        channel_name = data_node.name
                        # print("    data_name {0}".format(channel_name))
                        prefix,_,name = channel_name.rpartition(":")
                        if name in self.__motor_names:
                            last_pos = data_node.get(-1)
                            self.__infos[name] = "{0:8.3f}".format(last_pos)
                        if channel_name == self.__trig_name:
                            self.__infos["trig"] = len(data_node)
                                
            # print last images acquired and saved
            for cam in self.__limas:
                loop = int((self.__infos["trig"]-1)/(info['technique']['scan']['tomo_n']*info['technique']['scan']['nb_tomo'])) + 1
                if info['technique']['scan']['no_saving_between_tomo']:
                    last_rec = cam.camera.last_img_recorded
                    last_saved = cam._proxy.last_image_saved + 1
                else: 
                    if cam.camera.last_img_recorded in [0,cam._proxy.acq_nb_frames]:
                        last_rec = self.__infos["trig"]
                    else:
                        last_rec = int((self.__infos["trig"]-1) / cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam.camera.last_img_recorded
                    if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                        last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                    else:
                        last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                diff_trigger = self.__infos["trig"] - last_rec
                self.__infos[cam.name] = "{0} (saved {1} diff {2} loop {3})".format(last_rec, last_saved, diff_trigger, loop)

            
            msg = ""
            for (name, value) in self.__infos.items():
                msg += "{0} {1}  ".format(BOLD(name), value)
                
            if len(msg):
                # erase line and go to beginning of line
                print('\033[2K\033[1G', end="")
                print(msg + "\r", end="")
            
            for cam in self.__limas:
                if info['technique']['scan']['no_saving_between_tomo']:
                    if cam._proxy.last_image_saved + 1 == 0:
                        self.__start_time = datetime.datetime.fromtimestamp(time.time())
                    last_saved = cam._proxy.last_image_saved + 1
                else:
                    if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                        last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                    else:
                        last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                        self.__start_time = datetime.datetime.fromtimestamp(time.time())
                        self.__last_saved = last_saved
                end_time = datetime.datetime.fromtimestamp(time.time())
                depth = cam._proxy.image_sizes[1]
                width = cam._proxy.image_sizes[2]
                height = cam._proxy.image_sizes[3]
                mbsize = depth*width*height/1024/1024
                diff_saved = last_saved - self.__last_saved
                dtime = end_time - self.__start_time
                dtime_in_sec = dtime.total_seconds()
                self.__data_saving_rate = int(mbsize*diff_saved/dtime_in_sec)
                if cam._proxy.saving_mode == 'AUTO_FRAME':
                    if cam.camera.last_img_recorded == cam._proxy.acq_nb_frames:
                        # erase line and go to beginning of line
                        print('\033[2K\033[1G', end="")
                        while last_saved != self.__infos["trig"]:
                            if info['technique']['scan']['no_saving_between_tomo']:
                                last_saved = cam._proxy.last_image_saved + 1
                            else: 
                                if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                                    last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                                else:
                                    last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                            end_time = datetime.datetime.fromtimestamp(time.time())
                            depth = cam._proxy.image_sizes[1]
                            width = cam._proxy.image_sizes[2]
                            height = cam._proxy.image_sizes[3]
                            mbsize = depth*width*height/1024/1024
                            diff_saved = last_saved - self.__last_saved
                            dtime = end_time - self.__start_time
                            dtime_in_sec = dtime.total_seconds()
                            self.__data_saving_rate = int(mbsize*diff_saved/dtime_in_sec)
                            msg = "Saving in progress... {0} saved {1} data saving rate {2} MB/s)".format(BOLD(cam.name), last_saved, self.__data_saving_rate)
                            print(msg + "\r", end="")   
                       
            return


        if self.__state == ScanState.STOPPING:
            # print last motor position
            msg = ""
            for motor in self.__motors:
                msg += "{0} {1:8.3f}  ".format(BOLD(motor.name), motor.position)
            if len(msg):
                print(msg + "\r", end="")
            return
                
class PcoTomoScanWatchdog(WatchdogCallback):
    
    def __init__(self, trigger_name=None, limas=list(), trig_difference=100):
        """
        watchdog_timeout -- is the maximum calling frequency of **on_timeout**
        method.
        """
        self.__trig_difference = trig_difference
        self.__watchdog_timeout = 20.0
        self.__limas = limas
        self.__trig_name = trigger_name
        self.__stop_reason = ''

    @property
    def timeout(self):
        return self.__watchdog_timeout

    def on_timeout(self):
        """
        This method is called when **watchdog_timeout** elapsed it means
        that no data event is received for the time specified by
        **watchdog_timeout**
        """
        for cam in self.__limas:
            #check if lima is saving    
            if cam.camera.last_img_recorded != cam._proxy.acq_nb_frames:
                self.__stop_reason = f'No data received since {self.__watchdog_timeout} seconds'
                raise Exception   

    def on_scan_new(self, scan, scan_info):
        """
        Called when scan is starting
        """
        self.__state = None
        self.__trig = 0
        self.__last_saved = 0
    
    def on_scan_data(self, data_events, nodes, scan_info):
        """
        Called when new data are emitted by the scan.  This method should
        raise en exception to stop the scan.  All exception will
        bubble-up exception the **StopIteration**.  This one will just
        stop the scan.
        """
        # look for scan state
        state = scan_info.get("state", None)
        if state != self.__state:
            self.__state = state
        
        if self.__state == ScanState.STARTING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    node = nodes.get(acqdev)
                    if is_zerod(node):
                        channel_name = node.name
                        # print("    data_name {0}".format(channel_name))
                        prefix,_,name = channel_name.rpartition(":")
                        if channel_name == self.__trig_name:
                            self.__trig = len(node)
                            
        
            for cam in self.__limas:
                loop = int((self.__trig-1)/cam._proxy.acq_nb_frames) + 1
                if scan_info['technique']['scan']['no_saving_between_tomo']:
                    last_rec = cam.camera.last_img_recorded
                else:
                    if cam.camera.last_img_recorded in [0,cam._proxy.acq_nb_frames]:
                        last_rec = self.__trig
                    else:
                        last_rec = int((self.__trig-1) / cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam.camera.last_img_recorded
                diff_trigger = self.__trig - last_rec
                if self.__trig_difference != -1 and diff_trigger >= self.__trig_difference:
                    self.__stop_reason = f"\nSYNCHRO LOST: loop {loop}\n \
                    Frame/Trigger difference exceed {self.__trig_difference} ({diff_trigger})\n"
                    raise Exception   
        return

    def on_scan_end(self,scan_info):
        if len(self.__stop_reason):
            print(BOLD(self.__stop_reason) + "\n")

              
