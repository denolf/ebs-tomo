from bliss.scanning.scan import ScanPreset
from bliss import setup_globals, global_map
from bliss.common import session

class PcoTomoShutterPreset(ScanPreset):
    
    def __init__(self, soft_shutter):
        
        self.soft_shutter = soft_shutter
        self.shtime = None
        self.mux = session.get_current_session().config.get('multiplexer_tomo')
        
    def prepare(self,scan):
        motor_master = scan.acq_chain.nodes_list[0]
        scan_info = scan.scan_info
        
        if scan_info['soft_shut_time'] > 0:
            accpos = motor_master._calculate_undershoot(scan_info['start_pos'])
            disp = scan_info['start_pos'] - accpos
            mottime = abs(disp / scan_info['speed'])
            self.shtime = mottime - scan_info['soft_shut_time']
        
    def start(self, scan):
        scan_info=scan.scan_info
        if scan_info['soft_shut_time'] > 0:
            self.soft_shutter.open()
            if self.shtime <= 0:
                time.sleep(-self.shtime)
                
        if scan_info['sync_shut_time'] > 0:
            self.mux.switch("SHMODE","MUSST")
    
    def stop(self, scan):  
        scan_info=scan.scan_info
        if scan_info['sync_shut_time'] > 0:
            self.mux.switch("SHMODE","SOFT")
        
        if scan_info['soft_shut_time'] > 0:
            self.soft_shutter.close()     

class PcoTomoSyncMusstPreset(ScanPreset):
    
    def __init__(self, tomo_musst):
        
        self.motor = tomo_musst.motor
        self.musst = tomo_musst.card
    
    def start(self, scan):
        
        self.musst.ABORT
        motchan = self.musst.get_channel_by_name(self.motor._Axis__name)   
        motchan.value = int(round((self.motor.position*self.motor.steps_per_unit+0.5)//1))
