import datetime
import tomo
from tomo.pcotomo.PcoTomoScan import PcoTomoScan
from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug
from bliss.scanning.group import Sequence
from tomo.Presets import DarkShutterPreset, FastShutterPreset, CommonHeaderPreset
from tomo.pcotomo.Presets import PcoTomoShutterPreset
from bliss.common.cleanup import cleanup, error_cleanup, axis as cleanup_axis

class PcoTomo:
    """
    Class to handle pcotomo acquisition
    
    Use PcoTomoScan object to create scan
    
    """
    
    def __init__(self, tomo, *args, **kwargs):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        tomo : Tomo object (ex: HrPcoTomo) 
            contains all info about tomo (hardware, parameters)
        tomo_scan : TomoScan object
            contains acquisition chain corresponding to scan type
        """
            
        # init logging
        self.name = tomo.tomo_name+".pcotomo"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        self.tomo = tomo
        tomo.tomo_scan = PcoTomoScan(tomo)
        log_info(self,"__init__() leaving")


    def estimate_scan_time(self,motor,start_pos,end_pos,nb_points):
        """
        Estimates scan duration
        """
    
        tomo_tools = self.tomo.tomo_tools
        
        ref_scan_time = tomo_tools.estimate_ref_scan_duration()
        
        dark_scan_time = tomo_tools.estimate_dark_scan_duration()
                  
        proj_scan_time = tomo_tools.continuous_scan_time(motor,start_pos) 
        
        return ref_scan_time + dark_scan_time + proj_scan_time
        
    def prepare(self):
        """
        Calculates scan parameters and contructs acquisition chain corresponding to scan type
        """
        
        self.tomo.tomo_scan.calculate_parameters(self.tomo.parameters.start_pos,
                                       self.tomo.parameters.trange,
                                       self.tomo.parameters.tomo_n,
                                       self.tomo.parameters.time,
                                       self.tomo.parameters.latency_time,
                                       self.tomo.parameters.ntomo)   
        
        self.tomo.tomo_tools.check_params()
            
        est_time_scan = self.estimate_scan_time(self.tomo.tomo_musst.motor,
                                                self.tomo.parameters.start_pos,
                                                self.tomo.parameters.start_pos + self.tomo.tomo_scan.in_pars['trange'],
                                                self.tomo.tomo_scan.in_pars['tomo_n'])
        
        #saving time
        if self.tomo.parameters.noread:
            est_time_scan += self.tomo.parameters.nloop*self.tomo.tomo_scan.in_pars['tomo_n']*self.tomo.tomo_scan.in_pars['scan_point_time']
        
        self.tomo.in_pars['estimated_time_scan'] = str(datetime.timedelta(seconds=est_time_scan))
    
        self.tomo.tomo_musst.sync_motor()
        
        
    def projection_scan(self, start_pos, end_pos, nb_trig, title, scan_sequence=None, header={}):
        
        log_info(self,"projection_scan() entering")
        
        # add image identification to the common image header
        header['image_key']     = 'projection'
        
        # add meta data
        meta_data = self.tomo.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info['scan'] = meta_data['technique']['scan']
        scan_info['detector'] = meta_data['technique']['detector']
        scan_info = {'technique' : scan_info}
            
        restore_list = (cleanup_axis.POS, cleanup_axis.VEL,)
        with error_cleanup(self.tomo.rotation_axis, restore_list=restore_list):
            
            proj_scan = None        
            proj_scan = self.tomo.tomo_scan.continuous_scan(start_pos, end_pos, nb_trig,  
                                                       self.tomo.tomo_scan.in_pars['exposure_time'], 
                                                       self.tomo.tomo_scan.in_pars['time'],
                                                       self.tomo.parameters.ntomo,
                                                       self.tomo.parameters.nloop, 
                                                       self.tomo.parameters.nwait, 
                                                       self.tomo.parameters.noread, 
                                                       self.tomo.parameters.start_turn, 
                                                       self.tomo.tomo_scan.in_pars['sync_shut_angle'],
                                                       self.tomo.parameters.user_output, 
                                                       self.tomo.parameters.trigger_type,
                                                       title, scan_info, 
                                                       save=True, run=False)
            
            # add common header preset
            header_preset = CommonHeaderPreset(self.tomo.tomo_ccd, header)
            proj_scan.add_preset(header_preset)
            
            # add shutter preset
            shutter_preset = PcoTomoShutterPreset(self.tomo.shutter,self.tomo.tomo_musst,self.tomo.sequence_preset.mux)
            proj_scan.add_preset(shutter_preset)
            
            # add to scan sequence when requested
            if scan_sequence != None:
                scan_sequence.add(proj_scan)    
                
            # run the projection scan
            proj_scan.run()
            
        log_info(self,"projection_scan() leaving")
        
    def run(self):
        
        #seq=Sequence(title=self.tomo.sequence, scan_info=self.tomo.meta_data.tomo_scan_info())
        seq=Sequence(title=self.tomo.sequence, scan_info='')
        with seq.sequence_context() as scan_seq:
            self.tomo.run_sequence(scan_seq)    
