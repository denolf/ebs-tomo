import datetime
import tomo
from tomo.TomoTools import TomoTools
from bliss.controllers.motor import CalcController
from tomo.pcotomo.PcoTomoMusst import TriggerType

class PcoTomoTools(TomoTools):

    def __init__(self, tomo):
        """
        Parameters
        ----------
        tomo_ccd : object
            tomo detector object
        parameters : ParametersWardrobe 
            tomo parameters
        reference : object
            tomo reference object
        tomo : Tomo object (ex: HrPcoTomo) 
            contains all info about tomo (hardware, parameters)
        """
        
        self.tomo = tomo
        self.tomo_ccd = tomo.tomo_ccd 
        self.reference = tomo.reference
        self.parameters = tomo.parameters
        
        
    def check_params(self):
        """
        Checks if scan parameters are consistant
        """
                
        if self.parameters.noread:
            totimg = self.parameters.tomo_n * self.parameters.nloop
        else:
            totimg = self.parameters.tomo_n
        
        if totimg > self.tomo_ccd.detector.camera.max_nb_images:
            raise Exception(f"Error, Could not acquire {totimg} frames in pco memory\n"
                            +f"Current max number of frames is {self.tomo_ccd.detector.camera.max_nb_images} \n")     
        
        if self.tomo.tomo_scan.in_pars['exposure_time'] <= 0:
            raise ValueError("Error, dead time + latency time > expo time ==> Change parameters") 
            
        print("<<<Parameters are consistant !>>>")
 
    def continuous_scan_time(self, motor, start_pos):
        """
        Estimates continuous motor scan time 
        """
        speed = self.tomo.tomo_scan.in_pars['trange'] / self.tomo.tomo_scan.in_pars['time']
        acctime = speed / motor.acceleration
        accdisp =  speed * acctime / 2
        
        undershoot,undershoot_start_margin = self.tomo.tomo_scan.calculate_undershoot(motor,start_pos,self.tomo.tomo_scan.in_pars['time'],
                                                                                 self.tomo.parameters.start_turn)
        disp = undershoot + undershoot_start_margin
        
        scan_time = self.mot_disp_time(motor,disp,motor.velocity) 

        disp += undershoot_start_margin + self.tomo.parameters.nloop*self.tomo.tomo_scan.in_pars['trange']  
            
        if self.tomo.parameters.nloop > 1:
            delta = (start_pos + self.tomo.tomo_scan.in_pars['trange'])%360
            if delta > 0:
                delta = 360 - delta  
            if self.tomo.parameters.trigger_type == TriggerType.TIME:
                delta = self.tomo.tomo_scan.in_pars['trange'] + delta
            if self.tomo.parameters.nwait < 0:
                if self.tomo.parameters.nwait < -1:
                    delta += abs(self.tomo.parameters.nwait) * 360 
            else:
                delta += self.tomo.parameters.nwait * 360
        else:
            delta = 0
            
        disp += delta*(self.tomo.parameters.nloop-1)
        
        scan_time += self.mot_disp_time(motor,disp,speed) 
        
        return scan_time  

    def estimate_ref_scan_duration(self):
        # ref motor
        ref_mot = self.reference.ref_motors[0]
        if isinstance(ref_mot.controller, CalcController):
            unit_time = 0.0
        else:
            init_pos = self.reference.parameters.in_beam_position[0]
            target_pos = init_pos + self.reference.parameters.out_of_beam_displacement[0]
            # back and forth
            unit_time = 2 * self.mot_disp_time(ref_mot,target_pos-init_pos,ref_mot.velocity)
            unit_time += 2 * self.reference.settle_time

        motor_time = 0
        ref_img = 0
            
        if self.parameters.ref_images_at_start:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
            
        if self.parameters.ref_images_at_end:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
                
        image_time = ref_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return motor_time + image_time
