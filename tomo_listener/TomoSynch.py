# -*- coding: utf-8 -*-
#
# This file is part of the TomoSynch project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" Synchronize Tomo scans with Tomwer

The service listens on a Bliss session to detect the start and end of
a tomography acquisition sequence. 
Some attributes are available for information on the running sequence.
"""

# tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(TomoSynch.additionnal_import) ENABLED START #

import sys
import time
import math
import gevent
import traceback

### import bliss to have gevent monkey-patching done
import bliss
import gevent.monkey

# revert subprocess monkey-patching
import subprocess

for _name, _subprocess_item in gevent.monkey.saved["subprocess"].items():
    setattr(subprocess, _name, _subprocess_item)
###

from bliss.data.node import get_session_node
from bliss.data.node import get_node
from bliss.data.events import EventType

# PROTECTED REGION END #    //  TomoSynch.additionnal_import

__all__ = ["TomoSynch", "main"]


class TomoSynch(Device):
    """
    The service listens on a Bliss session to detect the start and end of
    a tomography acquisition sequence. 
    Some attributes are available for information on the running sequence.

    **Properties:**

    - Device Property
        session
            - The Bliss session to listen to
            - Type:'DevString'
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(TomoSynch.class_variable) ENABLED START #
    
    green_mode = tango.GreenMode.Gevent
    
    #
    # Blocking loop, listening to Bliss events
    #
    def listen_tomo_events(self):
        # Keep the event listening running as long as the server runs
        while(1):
            try:
                self.info_stream(f"started event listening on session {self.session}")
                print(f"started event listening on session {self.session}")
                
                for event_type, node, event_data in get_session_node(self.session).iterator.walk_on_new_events(filter=['scan_group','scan']):
                    self.debug_stream(f"Found new node: {node.db_name} {node.info['scan_nb']} {node.type} {event_type.name}")
                    
                    if event_type == EventType.NEW_NODE and node.type == 'scan_group':
                        self.debug_stream(f"-> start of sequence {node.db_name}")
                        
                        try:
                            # tomo sequences must have specific scan information
                            self.__sequence_title = node.info['technique']['scan']['sequence']
                            self.__tomo_n         = node.info['technique']['scan']['tomo_n']
                            self.info_stream ("tomo sequence = %s" % node.info['technique']['scan']['sequence'])
                            self.info_stream ("scan points   = %d" % node.info['technique']['scan']['tomo_n'])
                            
                            self.__saving_file          = node.info['filename']
                            self.__sequence_scan_number = node.info['scan_nb']
                            self.info_stream ("sequence saving = %s" % node.info['filename'])
                            self.info_stream ("sequence number = %d" % node.info['scan_nb'])
                            
                            self.__tomo_state = tango.DevState.MOVING
                            self.__start_time = time.ctime()
                            
                            # reset all variables to avoid reading of data from the last sequence
                            self.__scan_title           = ""
                            self.__scan_number          = -1
                            self.__end_time             = ""
            
                        except:
                            #print ("No tomo sequence!")
                            ex_info = sys.exc_info()
                            try:
                                tango.Except.throw_exception(str(ex_info[0]), str(ex_info[1]), traceback.format_exc())
                            except tango.DevFailed as ex:
                                tango.Except.print_exception(ex)
                            
                            self.__tomo_state = tango.DevState.ON
                    
                    if self.__tomo_state == tango.DevState.MOVING:
                        # started scan in the sequence
                        if event_type == EventType.NEW_NODE and node.type == 'scan':
                            self.debug_stream(f"-> start of scan  {node.db_name}")
                            
                            self.__scan_title  = node.info['title']
                            self.__scan_number = node.info['scan_nb']
                            self.info_stream ("scan title  = %s" % node.info['title'])
                            self.info_stream ("scan number = %d" % node.info['scan_nb'])
                            
                        # ended scan in the sequence
                        if event_type == EventType.END_SCAN and node.type == 'scan':
                            self.debug_stream(f"-> another scan in sequence done: {node.db_name}")
                        
                        # ended tomo sequence
                        if event_type == EventType.END_SCAN and node.type == 'scan_group':
                            self.debug_stream(f"-> end of sequence {node.db_name}" ) 
                            
                            self.__tomo_state = tango.DevState.ON
                            self.__end_time = time.ctime()
            
            # catch all exceptions, print the traceback to the console and
            # add the information to the device status until acknowledgement
            except:
                ex_info = sys.exc_info()
                try:
                    tango.Except.throw_exception(str(ex_info[0]), str(ex_info[1]), traceback.format_exc())
                except tango.DevFailed as ex:
                    self.task_error = ex
                    tango.Except.print_exception(ex)
                        
                    self.is_error   = True
                    self.__tomo_state = tango.DevState.FAULT
                    self.__end_time = time.ctime()
    
    
    # PROTECTED REGION END #    //  TomoSynch.class_variable

    # -----------------
    # Device Properties
    # -----------------

    session = device_property(
        dtype='DevString',
        mandatory=True
    )

    # ----------
    # Attributes
    # ----------

    SequenceTitle = attribute(
        dtype='DevString',
    )

    SequenceScanNumber = attribute(
        dtype='DevLong',
    )

    SavingFile = attribute(
        dtype='DevString',
    )

    TomoN = attribute(
        dtype='DevDouble',
    )

    ScanTitle = attribute(
        dtype='DevString',
    )

    ScanNumber = attribute(
        dtype='DevLong',
    )

    StartTime = attribute(
        dtype='DevString',
    )

    EndTime = attribute(
        dtype='DevString',
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the TomoSynch."""
        Device.init_device(self)
        # PROTECTED REGION ID(TomoSynch.init_device) ENABLED START #
        
        # init flags for state and status
        self.init_done      = False
        
        self.is_error       = False
        self.task_error     = None
        
        self.__sequence_title       = ""
        self.__sequence_scan_number = -1
        self.__saving_file          = ""
        self.__tomo_n               = -1
        self.__scan_title           = ""
        self.__scan_number          = -1
        self.__start_time           = ""
        self.__end_time             = ""
        
        self.__tomo_state = tango.DevState.ON
        self.__status = ""
        
        # start the loop to listen to Bliss events
        self.running_task = gevent.spawn(self.listen_tomo_events)
        
        if self.__tomo_state != tango.DevState.FAULT:
            self.init_done = True
        
        # PROTECTED REGION END #    //  TomoSynch.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(TomoSynch.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSynch.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(TomoSynch.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  TomoSynch.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_SequenceTitle(self):
        # PROTECTED REGION ID(TomoSynch.SequenceTitle_read) ENABLED START #
        """Return the SequenceTitle attribute."""
        return self.__sequence_title
        # PROTECTED REGION END #    //  TomoSynch.SequenceTitle_read

    def read_SequenceScanNumber(self):
        # PROTECTED REGION ID(TomoSynch.SequenceScanNumber_read) ENABLED START #
        """Return the SequenceScanNumber attribute."""
        return self.__sequence_scan_number
        # PROTECTED REGION END #    //  TomoSynch.SequenceScanNumber_read

    def read_SavingFile(self):
        # PROTECTED REGION ID(TomoSynch.SavingFile_read) ENABLED START #
        """Return the SavingFile attribute."""
        return self.__saving_file
        # PROTECTED REGION END #    //  TomoSynch.SavingFile_read

    def read_TomoN(self):
        # PROTECTED REGION ID(TomoSynch.TomoN_read) ENABLED START #
        """Return the TomoN attribute."""
        return self.__tomo_n
        # PROTECTED REGION END #    //  TomoSynch.TomoN_read

    def read_ScanTitle(self):
        # PROTECTED REGION ID(TomoSynch.ScanTitle_read) ENABLED START #
        """Return the ScanTitle attribute."""
        return self.__scan_title
        # PROTECTED REGION END #    //  TomoSynch.ScanTitle_read

    def read_ScanNumber(self):
        # PROTECTED REGION ID(TomoSynch.ScanNumber_read) ENABLED START #
        return self.__scan_number
        # PROTECTED REGION END #    //  TomoSynch.ScanNumber_read

    def read_StartTime(self):
        # PROTECTED REGION ID(TomoSynch.StartTime_read) ENABLED START #
        return self.__start_time
        # PROTECTED REGION END #    //  TomoSynch.StartTime_read

    def read_EndTime(self):
        # PROTECTED REGION ID(TomoSynch.EndTime_read) ENABLED START #
        """Return the EndTime attribute."""
        return self.__end_time
        # PROTECTED REGION END #    //  TomoSynch.EndTime_read

    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(TomoSynch.State) ENABLED START #
        
        self.set_state(self.__tomo_state)
        return self.__tomo_state
        
        # PROTECTED REGION END #    //  TomoSynch.State

    @DebugIt()
    def dev_status(self):
        # PROTECTED REGION ID(TomoSynch.Status) ENABLED START #
        
        self.__status = "Tomo sequence "
        if self.__tomo_state == tango.DevState.FAULT:
            self.__status += "failed"
        else:
            if self.__tomo_state == tango.DevState.MOVING:
                self.__status += "running"
                self.__status += "\n\n" + self.last_task
            else:
                if self.__tomo_state == tango.DevState.ON:
                    self.__status += "not running"
                else:
                    self.__status += "UNKNOWN"
        self.__status += "\n"

        if self.__tomo_state == tango.DevState.FAULT:
            if self.is_error:
                # Get the task exception stored
                if self.task_error != None:
                    self.__status += "\n Exception:\n"
                    self.__status += str(self.task_error)
        
        self.set_status(self.__status)
        return self.__status
        
        # PROTECTED REGION END #    //  TomoSynch.Status

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the TomoSynch module."""
    # PROTECTED REGION ID(TomoSynch.main) ENABLED START #
    
    # Enable gevents for the server
    kwargs.setdefault("green_mode", tango.GreenMode.Gevent)
    
    return run((TomoSynch,), args=args, **kwargs)
    # PROTECTED REGION END #    //  TomoSynch.main


if __name__ == '__main__':
    main()
